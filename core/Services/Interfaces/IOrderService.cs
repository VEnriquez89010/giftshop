﻿using core.Model;

namespace core.Services
{
    public interface IOrderService
    {
        bool Create(Order order);
    }
}