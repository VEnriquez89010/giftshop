﻿using core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace core.Services
{

    public class CustomerService : ICustomerService
    {
        public ICustomerRepository _customerRepository;

        public CustomerService( ICustomerRepository customerRepository )
        {
            _customerRepository = customerRepository;
        }

        public int isAdmin(int id)
        {
          return  _customerRepository.IsAdmin( id );
        }
    }
}
