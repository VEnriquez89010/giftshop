﻿using core.Model;
using core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Services
{
    public class OrderService : IOrderService
    {
        public IOrderRepository _orderRepository;

            public OrderService( IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public bool Create( Order order )
        {
           return  _orderRepository.Create( order );
        }
    }
}
