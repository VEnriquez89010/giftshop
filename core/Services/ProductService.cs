﻿using core.Model;
using core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Services
{
    public class ProductService : IProductService
    {

        public IProductRepository _productRepository;

        public ProductService( IProductRepository productRepository )
        {
            _productRepository = productRepository;
        }

        public ICollection<Product> GetAll() {
            return _productRepository.All();
        }

        public Product GetById( int id )
        {
            if( id > 0 ) return _productRepository.FindById( id );
            return null;
        }

        public ICollection<Product> Search( string category )
        {
            return _productRepository.Search(category);
        }

        public ICollection<Product> Search( string category , string keyword )
        {
            return _productRepository.Search( category , keyword);
        }

        public void AddProduct( Product product )
        {
            _productRepository.Add( product );
        }

        public void Remove(int id)
        {
            _productRepository.Remove( id );
        }

        public void Edit(Product product)
        {
            _productRepository.Edit( product );
        }

    }
}
