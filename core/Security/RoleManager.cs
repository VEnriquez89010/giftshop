using Microsoft.AspNet.Identity;
using SS.Mvc.core.Model;

namespace SS.Mvc.core.Security
{
    public class RoleManager : RoleManager<Role, int>, IRoleManager
    {
        public RoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }
    }
}