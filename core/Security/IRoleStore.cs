using System.Linq;
using System.Threading.Tasks;
using SS.Mvc.core.Model;

namespace SS.Mvc.core.Security
{
    public interface IRoleStore
    {
        Task CreateAsync(Role role);
        Task DeleteAsync(Role role);
        Task<Role> FindByIdAsync(int roleId);
        Task<Role> FindByNameAsync(string roleName);
        Task UpdateAsync(Role role);
    }
}