namespace core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CartProducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CartProduct",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Details = c.String(),
                        ImageUrl = c.String(),
                        Stock = c.Int(nullable: false),
                        Category = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderCartProduct",
                c => new
                    {
                        Order_Id = c.Int(nullable: false),
                        CartProduct_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Order_Id, t.CartProduct_Id })
                .ForeignKey("dbo.Order", t => t.Order_Id)
                .ForeignKey("dbo.CartProduct", t => t.CartProduct_Id)
                .Index(t => t.Order_Id)
                .Index(t => t.CartProduct_Id);
            
            AddColumn("dbo.Product", "Category", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderCartProduct", "CartProduct_Id", "dbo.CartProduct");
            DropForeignKey("dbo.OrderCartProduct", "Order_Id", "dbo.Order");
            DropIndex("dbo.OrderCartProduct", new[] { "CartProduct_Id" });
            DropIndex("dbo.OrderCartProduct", new[] { "Order_Id" });
            DropColumn("dbo.Product", "Category");
            DropTable("dbo.OrderCartProduct");
            DropTable("dbo.Order");
            DropTable("dbo.CartProduct");
        }
    }
}
