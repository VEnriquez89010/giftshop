namespace core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderTotal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "Total", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order", "Total");
        }
    }
}
