namespace core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderTimestamp : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Order", "Created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "Created", c => c.DateTime(nullable: false));
        }
    }
}
