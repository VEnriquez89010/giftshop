namespace core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderProduct : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Order", "Created");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order", "Created", c => c.DateTime(nullable: false));
        }
    }
}
