﻿using SS.Model;
using SS.Mvc.core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Model
{
    public class Order : Entity
    {
        public virtual List<CartProduct> Products { get; set; }
        public int UserId { get; set; }

        public long Total { get; set; }
        public DateTime Created { get; set; }
    }
}
