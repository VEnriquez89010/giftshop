﻿using SS.Data.PetaPoco;
using SS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Model
{
    public class CartProduct : Entity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Details { get; set; }
        public string ImageUrl { get; set; }

        [Column("Quantity")]
        public int Stock { get; set; }
        public string Category { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}
