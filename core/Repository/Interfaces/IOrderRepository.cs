﻿using core.Model;

namespace core.Repository
{
    public interface IOrderRepository
    {
        bool Create(Order order);
    }
}