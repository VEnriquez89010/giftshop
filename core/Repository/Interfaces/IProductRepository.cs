﻿using System.Collections.Generic;
using core.Model;

namespace core.Repository
{
    public interface IProductRepository
    {
        ICollection<Product> All();
        Product FindById(int id);
        ICollection<Product> Search( string category , string keyword);
         ICollection<Product> Search( string category);
        void Add(Product product);
        void Remove( int id );
        void Edit(Product product);

    }
}