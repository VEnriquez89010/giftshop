﻿using core.Model;
using SS.Data.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Repository
{
    class OrderRepository : IOrderRepository
    {

        private readonly IWorkspace _workspace;

        public OrderRepository( IWorkspace workspace )
        {
            _workspace = workspace;
        }

        public bool Create( Order order )
        {
            try
            {
                order.Created = DateTime.Now;

                //---Updating Stock Products
                if( !updateStock ( order )) return false;

                _workspace.Add<Order>( order );
                _workspace.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        private bool updateStock ( Order order )
        {
            foreach ( CartProduct cartProduct in order.Products )
            {
                Product product = _workspace.Single<Product>( x => x.Name.Equals( cartProduct.Name ));
                if ( product == null ) return false;
                product.Stock -= cartProduct.Stock;
                if ( product.Stock < 0 ) return false;
                _workspace.Update<Product>( product );
                _workspace.SaveChanges();
            }
            return true;
        }
    }
}
