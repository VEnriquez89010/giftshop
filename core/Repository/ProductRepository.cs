﻿using core.Model;
using SS.Data.EntityFramework;
using SS.Mvc.core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Repository
{
    class ProductRepository : IProductRepository
    {

        private readonly IWorkspace _workspace;

        public ProductRepository( IWorkspace workspace )
        {
            _workspace = workspace;
        }
        public ICollection<Product> All()
        {
            return _workspace.List<Product>();
        }

        public Product FindById( int id )
        {
            return _workspace.Single<Product>( x => x.Id.Equals( id ));
        }

        public void  Edit( Product product )
        {
            _workspace.Update<Product>( product );
            _workspace.SaveChanges();
        }

        public ICollection<Product> Search(   string category , string keyword )
        {
            if ( category.Equals( "all" )) return _workspace.List<Product>( x => x.Name.ToLower().Contains( keyword ) || x.Details.ToLower().Contains( keyword ));
            return _workspace.List<Product>(x => x.Category.Equals( category ) && ( x.Name.ToLower().Contains( keyword ) || x.Details.ToLower().Contains( keyword )));

        }

        public ICollection<Product> Search( string category )
        {
            if( category.Equals( "all" )) return _workspace.List<Product>();
            return _workspace.List<Product>( x => x.Category.Equals( category ));
        }

        public void Add( Product product )
        {
            _workspace.Add<Product>( product );
            _workspace.SaveChanges();
        }

        public void Remove( int id )
        {
            _workspace.Delete<Product>( x => x.Id.Equals( id ));
            _workspace.SaveChanges();
        }
    }
}
