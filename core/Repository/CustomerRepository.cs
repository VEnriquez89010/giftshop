﻿using SS.Data.EntityFramework;
using SS.Mvc.core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace core.Repository
{
    class CustomerRepository : ICustomerRepository
    {
        private readonly IWorkspace _workspace;

        public CustomerRepository(IWorkspace workspace)
        {
            _workspace = workspace;
        }
        public int IsAdmin( int id )
        {
            var userRole = _workspace.Single<UserRole>(x => x.UserId.Equals( id ));
            return userRole.RoleId;
        }

       
    }
}
