﻿
using SS.Mvc.core.Model;
using System.Data.Entity.ModelConfiguration;

namespace SS.Mvc.core.Data.Mappings
{
    internal sealed class UserRoleMapping : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMapping()
        {
            HasKey(x => new { x.UserId, x.RoleId });
        }
    }
}