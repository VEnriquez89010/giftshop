﻿using System;
using System.Web.Mvc;

namespace SS.Mvc.Template1.Controllers
{
   
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //return View();
            return new FilePathResult("~/public/index.html" , "text/html");
        }

#if DEBUG

        [System.Diagnostics.DebuggerStepThrough]
		public ActionResult ErrorTest()
		{
			throw new ApplicationException("Error test.");
		}

#endif
    }
}