﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;

namespace SS.Mvc.Template1.Controllers
{
    [RoutePrefix("mail")]
    public class MailTokenController : ApiController
    {
        //private const string ResetPasswordTokenPurpose = "RP";
        //private const string ConfirmEmailTokenPurpose = "EC";//change here change bit length for reason  section (2 per char)

        //public void GenerateTokenTest()
        //{
        //    MyUser user = CreateTestUser("name");
        //    user.Id = 123;
        //    user.SecurityStamp = Guid.NewGuid().ToString();
        //    var token = sit.GenerateToken(ConfirmEmailTokenPurpose, user);
        //    var validation = sit.ValidateToken(ConfirmEmailTokenPurpose, user, token);
        //    Assert.IsTrue(validation.Validated, "Token validated for user 123");
        //}

        [HttpGet]
        [Route("generate/{id}/{reason}")]
        public IHttpActionResult GenerateToken(string id ,  string reason)
        {
            char[] padding = { '=' };
            byte[] _time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] _key = Guid.Parse("8240313e-615d-49ed-99fb-841187a153ba").ToByteArray();
            byte[] _Id = Encoding.ASCII.GetBytes(id);
            byte[] _reason = Encoding.ASCII.GetBytes(reason);
            byte[] data = new byte[_time.Length + _key.Length + _reason.Length + _Id.Length];

            Buffer.BlockCopy(_time, 0, data, 0, _time.Length);
            Buffer.BlockCopy(_key, 0, data, _time.Length, _key.Length);
            Buffer.BlockCopy(_reason, 0, data, _time.Length + _key.Length, _reason.Length);
            Buffer.BlockCopy(_Id, 0, data, _time.Length + _key.Length + _reason.Length, _Id.Length);

            var token = Convert.ToBase64String(data.ToArray()) .TrimEnd(padding).Replace('+', '-').Replace('/', '_');

            return Ok( token );
        }

        [HttpGet]
        [Route("validate/{id}/{reason}/{token}")]
        public IHttpActionResult ValidateToken(string id , string reason , string token)
        {
            string incoming = token.Replace('_', '/').Replace('-', '+');
            switch (token.Length % 4)
            {
                case 2: incoming += "=="; break;
                case 3: incoming += "="; break;
            }

            var result = new TokenValidation();
            byte[] data = Convert.FromBase64String(incoming);
            byte[] _time = data.Take( 8 ).ToArray();
            byte[] _key = data.Skip( 8 ).Take( 16 ).ToArray();
            byte[] _reason = data.Skip( 24 ).Take( 4 ).ToArray();
            byte[] _Id = data.Skip( 28 ).ToArray();

            var gKey = new Guid(_key);
            var when = DateTime.FromBinary( BitConverter.ToInt64( _time , 0 ));

            if ( when < DateTime.UtcNow.AddHours( -24 ))  result.Errors.Add( TokenValidationStatus.Expired );
            if ( gKey.ToString() != "8240313e-615d-49ed-99fb-841187a153ba" ) result.Errors.Add( TokenValidationStatus.WrongGuid );
            if ( reason != Encoding.UTF8.GetString( _reason )) result.Errors.Add( TokenValidationStatus.WrongPurpose );
            if ( id != Encoding.UTF8.GetString( _Id )) result.Errors.Add( TokenValidationStatus.WrongUser );

            return Ok( result );
        }

        public class TokenValidation
        {
            public bool Validated { get { return Errors.Count == 0; } }
            public readonly List<TokenValidationStatus> Errors = new List<TokenValidationStatus>();
        }

        public enum TokenValidationStatus
        {
            Expired,
            WrongUser,
            WrongPurpose,
            WrongGuid
        }
    }
}