﻿using core.Model;
using core.Services;
using Microsoft.AspNet.Identity;
using SS.Mvc.core.Data;
using SS.Mvc.core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SS.Mvc.Template1.Controllers
{
    [RoutePrefix("Orders")]
    public class OrderController : ApiController
    {

        public IOrderService _orderService;

        public OrderController( IOrderService orderService )
        {
            _orderService = orderService;
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create([FromBody] Order order)
        {
            string redirect = null;
            redirect = ( order.Products != null && order.Products.Any() && _orderService.Create( order )) ? "/thankyou" : "/msg" ;
            return Ok( redirect );
        }
     
    }
}