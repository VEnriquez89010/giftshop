﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SS.Mvc.Template1.Controllers
{
    public class ViewsController : Controller
    {
        private const string HtmlExtension = ".html";

        // This will handle all AJAX requests for Angular views
        protected override void HandleUnknownAction(string actionName)
        {
#if !DEBUG
            if (Request.IsAjaxRequest())
#endif
            {
                var action = RouteData.GetRequiredString("action");

                if (!(string.IsNullOrEmpty(action) || action.StartsWith("_", StringComparison.Ordinal)))
                {
                    if (action.EndsWith(HtmlExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        action = action.Substring(0, action.Length - HtmlExtension.Length);
                    }

                    action = action.Replace("/", "_");

                    var result = ViewEngines.Engines.FindPartialView(ControllerContext, action);
                    if (result?.View != null && result.ViewEngine != null)
                    {
                        var viewContext = new ViewContext(ControllerContext, result.View, ViewData, TempData, Response.Output);
                        result.View.Render(viewContext, Response.Output);
                        result.ViewEngine.ReleaseView(ControllerContext, result.View);
                        return;
                    }
                }
            }

            base.HandleUnknownAction(actionName);
        }
    }
}