﻿using core.Model;
using SS.Data.EntityFramework;
using SS.Mvc.core.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Net;
using core.Services;

namespace SS.Mvc.Template1.Controllers
{
    [RoutePrefix("Products")]
    public class ProductController : ApiController
    {
        public IProductService _productService;

        public ProductController( IProductService productService )
        {
            _productService = productService;
        }
       
        [HttpGet]
        [Route("all")]
        public IHttpActionResult all()
        {
            return Ok( _productService.GetAll() );
        }

        [HttpGet]
        [Route("productImages")]
        public IHttpActionResult productImages()
        {
            List<Product> productImages = new List<Product>();
            productImages = convertImages( _productService.GetAll() );
            return Ok(productImages);
        }

        [HttpGet]
        [Route("search/{category}/{keyword}")]
        public IHttpActionResult search( string category , string keyword )
        {
            List<Product> productImages = new List<Product>();
            ICollection<Product> products = (keyword == "none") ? _productService.Search(category) : _productService.Search(category, keyword);
            productImages = convertImages( products );
            return Ok(productImages);
        }

        [HttpPost]
        [Route("edit")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Edit([FromBody] Product product )
        {
            if ( validate( product )) _productService.Edit(product);
            return redirect();
        }

        [HttpGet]
        [Route("remove/{id:int}")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Remove( int id )
        {
            if ( id.GetType() == typeof( int ) ) _productService.Remove( id );
            return redirect();
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Add([FromBody] Product product )
        {
            if (validate( product )) _productService.AddProduct( product );
            return redirect();
        }

        [HttpGet]
        [Route("findById/{id:int}")]
        public IHttpActionResult FindById( int id )
        {
            if ( id > 0 && id.GetType() == typeof( int ) ) return Ok( convertImage( _productService.GetById( id )));
            return null;
        }

        public HttpResponseMessage redirect()
        {
            var response = Request.CreateResponse(HttpStatusCode.Moved);
            response.Headers.Location = new Uri("http://localhost:58702/DZf1rQHSBkBq4J0qDd54ng");
            return response;

        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("uploadFile")]
        public HttpResponseMessage uploadFile()
        {
            var httpRequest = HttpContext.Current.Request;
            string filePathImage = "";
            if ( httpRequest.Files.Count < 1) return Request.CreateResponse(HttpStatusCode.BadRequest);

            foreach ( string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                string path = "C:/ProductsImage/";
                filePathImage = DateTime.Now.ToString("MMddyyyyHmmss") + " " + postedFile.FileName;

                var filePath = path + filePathImage;
                postedFile.SaveAs(filePath);
            }
            return Request.CreateResponse(HttpStatusCode.Created , filePathImage );
        }

        private List<Product> convertImages( ICollection<Product> products )
        {
            List<Product> productImages = new List<Product>();
            foreach (Product product in products)
            {
                byte[] imageArray = System.IO.File.ReadAllBytes(@"C:\ProductsImage\" + product.ImageUrl);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                product.ImageUrl = base64ImageRepresentation;
                productImages.Add(product);
            }
            return productImages;
        }

        private Product convertImage( Product product )
        {
            Product productImage = new Product();
            byte[] imageArray = System.IO.File.ReadAllBytes(@"C:\ProductsImage\" + product.ImageUrl);
            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            product.ImageUrl = base64ImageRepresentation;
            return product;
        }

        private bool validate( Product product )
        {
            if ( product == null ) return false;
            if ( product.Name == null ) return false;
            if ( product.Category == null ) return false;
            if ( product.Details == null ) return false;
            if ( product.ImageUrl == null ) return false;
            if ( product.Price.GetType() != typeof( decimal ) ) return false;
            if ( product.Stock.GetType() != typeof( int )) return false;
            if ( product.Id.GetType() != typeof( int )) return false;
            return true;
        }

    }
}