﻿using core.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SS.Mvc.Template1.Controllers
{
    [RoutePrefix("Customers")]
    public class CustomerController : ApiController
    {
        public ICustomerService _customerService;

        public CustomerController( ICustomerService customerService )
        {
            _customerService = customerService;
        }

        [HttpGet]
        [Route("isLoged")]
        public IHttpActionResult isLoged()
        {
            string name = null;
            if ( User.Identity != null ) name = User.Identity.Name;
            return Ok( name );
        }

        [HttpGet]
        [Route("isAdmin")]
        public IHttpActionResult isAdmin()
        {
            int userId = 0;
            if ( User.Identity != null && User.Identity.GetUserId() != null) userId = int.Parse( User.Identity.GetUserId() );
            if ( userId == 0 ) return Ok();
            return Ok( _customerService.isAdmin( userId ));
        }

        [HttpGet]
        [Route("UserId")]
        public string userId()
        {
            string UserId = null;
            if (User.Identity != null) UserId = User.Identity.GetUserId();
            return UserId;
        }

        [HttpGet]
        [Route("admin")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Admin()
        {
            var response = Request.CreateResponse(HttpStatusCode.Moved);
            response.Headers.Location = new Uri("http://localhost:58702/DZf1rQHSBkBq4J0qDd54ng");
            return response;
        }

    }
}