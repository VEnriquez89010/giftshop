(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-wrapper {\r\n    background: #fff;\r\n    padding: 20px 25px;\r\n    margin: 30px 0;\r\n    border-radius: 3px;\r\n    box-shadow: 0 1px 1px rgba(0,0,0,.05);\r\n}\r\n.table-title {        \r\n    padding-bottom: 15px;\r\n    background: #435d7d;\r\n    color: #fff;\r\n    padding: 16px 30px;\r\n    margin: -20px -25px 10px;\r\n    border-radius: 3px 3px 0 0;\r\n}\r\n.table-title h2 {\r\n    margin: 5px 0 0;\r\n    font-size: 24px;\r\n}\r\n.table-title .btn-group {\r\n    float: right;\r\n}\r\n.table-title .btn {\r\n    color: #fff;\r\n    float: right;\r\n    border: none;\r\n    min-width: 50px;\r\n    border-radius: 2px;\r\n    border: none;\r\n    outline: none !important;\r\n    margin-left: 10px;\r\n}\r\n.table-title .btn i {\r\n    float: left;\r\n    font-size: 21px;\r\n    margin-right: 5px;\r\n}\r\n.table-title .btn span {\r\n    float: left;\r\n    margin-top: 2px;\r\n}\r\ntable.table tr th, table.table tr td {\r\n    border-color: #e9e9e9;\r\n    padding: 12px 15px;\r\n    vertical-align: middle;\r\n}\r\ntable.table tr th:first-child {\r\n    width: 60px;\r\n}\r\ntable.table tr th:last-child {\r\n    width: 100px;\r\n}\r\ntable.table-striped tbody tr:nth-of-type(odd) {\r\n    background-color: #fcfcfc;\r\n}\r\ntable.table-striped.table-hover tbody tr:hover {\r\n    background: #f5f5f5;\r\n}\r\ntable.table th i {\r\n    font-size: 13px;\r\n    margin: 0 5px;\r\n    cursor: pointer;\r\n}\r\ntable.table td:last-child i {\r\n    opacity: 0.9;\r\n    font-size: 22px;\r\n    margin: 0 5px;\r\n}\r\ntable.table td a {\r\n    font-weight: bold;\r\n    color: #566787;\r\n    display: inline-block;\r\n    text-decoration: none;\r\n    outline: none !important;\r\n}\r\ntable.table td a:hover {\r\n    color: #2196F3;\r\n}\r\ntable.table td a.edit {\r\n    color: #FFC107;\r\n}\r\ntable.table td a.delete {\r\n    color: #F44336;\r\n}\r\ntable.table td i {\r\n    font-size: 19px;\r\n}\r\ntable.table .avatar {\r\n    border-radius: 50%;\r\n    vertical-align: middle;\r\n    margin-right: 10px;\r\n}\r\n.fileContainer {\r\n    overflow: hidden;\r\n    position: relative;\r\n}\r\n.fileContainer [type=file] {\r\n    cursor: inherit;\r\n    display: block;\r\n    font-size: 999px;\r\n    filter: alpha(opacity=0);\r\n    min-height: 100%;\r\n    min-width: 100%;\r\n    opacity: 0;\r\n    position: absolute;\r\n    right: 0;\r\n    text-align: right;\r\n    top: 0;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vYWRtaW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixzQ0FBc0M7Q0FDekM7QUFDRDtJQUNJLHFCQUFxQjtJQUNyQixvQkFBb0I7SUFDcEIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsMkJBQTJCO0NBQzlCO0FBQ0Q7SUFDSSxnQkFBZ0I7SUFDaEIsZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxhQUFhO0NBQ2hCO0FBQ0Q7SUFDSSxZQUFZO0lBQ1osYUFBYTtJQUNiLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYix5QkFBeUI7SUFDekIsa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksWUFBWTtJQUNaLGdCQUFnQjtDQUNuQjtBQUNEO0lBQ0ksc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQix1QkFBdUI7Q0FDMUI7QUFDRDtJQUNJLFlBQVk7Q0FDZjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksMEJBQTBCO0NBQzdCO0FBQ0Q7SUFDSSxvQkFBb0I7Q0FDdkI7QUFDRDtJQUNJLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGNBQWM7Q0FDakI7QUFDRDtJQUNJLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLHNCQUFzQjtJQUN0Qix5QkFBeUI7Q0FDNUI7QUFDRDtJQUNJLGVBQWU7Q0FDbEI7QUFDRDtJQUNJLGVBQWU7Q0FDbEI7QUFDRDtJQUNJLGVBQWU7Q0FDbEI7QUFDRDtJQUNJLGdCQUFnQjtDQUNuQjtBQUNEO0lBQ0ksbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixtQkFBbUI7Q0FDdEI7QUFFRDtJQUNJLGlCQUFpQjtJQUNqQixtQkFBbUI7Q0FDdEI7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsU0FBUztJQUNULGtCQUFrQjtJQUNsQixPQUFPO0NBQ1YiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi9hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYmxlLXdyYXBwZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDIwcHggMjVweDtcclxuICAgIG1hcmdpbjogMzBweCAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgYm94LXNoYWRvdzogMCAxcHggMXB4IHJnYmEoMCwwLDAsLjA1KTtcclxufVxyXG4udGFibGUtdGl0bGUgeyAgICAgICAgXHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgIGJhY2tncm91bmQ6ICM0MzVkN2Q7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDE2cHggMzBweDtcclxuICAgIG1hcmdpbjogLTIwcHggLTI1cHggMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweCAzcHggMCAwO1xyXG59XHJcbi50YWJsZS10aXRsZSBoMiB7XHJcbiAgICBtYXJnaW46IDVweCAwIDA7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbn1cclxuLnRhYmxlLXRpdGxlIC5idG4tZ3JvdXAge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi50YWJsZS10aXRsZSAuYnRuIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgbWluLXdpZHRoOiA1MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuLnRhYmxlLXRpdGxlIC5idG4gaSB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcbi50YWJsZS10aXRsZSAuYnRuIHNwYW4ge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBtYXJnaW4tdG9wOiAycHg7XHJcbn1cclxudGFibGUudGFibGUgdHIgdGgsIHRhYmxlLnRhYmxlIHRyIHRkIHtcclxuICAgIGJvcmRlci1jb2xvcjogI2U5ZTllOTtcclxuICAgIHBhZGRpbmc6IDEycHggMTVweDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxudGFibGUudGFibGUgdHIgdGg6Zmlyc3QtY2hpbGQge1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbn1cclxudGFibGUudGFibGUgdHIgdGg6bGFzdC1jaGlsZCB7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbn1cclxudGFibGUudGFibGUtc3RyaXBlZCB0Ym9keSB0cjpudGgtb2YtdHlwZShvZGQpIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmY2ZjZmM7XHJcbn1cclxudGFibGUudGFibGUtc3RyaXBlZC50YWJsZS1ob3ZlciB0Ym9keSB0cjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xyXG59XHJcbnRhYmxlLnRhYmxlIHRoIGkge1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgbWFyZ2luOiAwIDVweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVx0XHJcbnRhYmxlLnRhYmxlIHRkOmxhc3QtY2hpbGQgaSB7XHJcbiAgICBvcGFjaXR5OiAwLjk7XHJcbiAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICBtYXJnaW46IDAgNXB4O1xyXG59XHJcbnRhYmxlLnRhYmxlIHRkIGEge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzU2Njc4NztcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG50YWJsZS50YWJsZSB0ZCBhOmhvdmVyIHtcclxuICAgIGNvbG9yOiAjMjE5NkYzO1xyXG59XHJcbnRhYmxlLnRhYmxlIHRkIGEuZWRpdCB7XHJcbiAgICBjb2xvcjogI0ZGQzEwNztcclxufVxyXG50YWJsZS50YWJsZSB0ZCBhLmRlbGV0ZSB7XHJcbiAgICBjb2xvcjogI0Y0NDMzNjtcclxufVxyXG50YWJsZS50YWJsZSB0ZCBpIHtcclxuICAgIGZvbnQtc2l6ZTogMTlweDtcclxufVxyXG50YWJsZS50YWJsZSAuYXZhdGFyIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5maWxlQ29udGFpbmVyIHtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5maWxlQ29udGFpbmVyIFt0eXBlPWZpbGVdIHtcclxuICAgIGN1cnNvcjogaW5oZXJpdDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC1zaXplOiA5OTlweDtcclxuICAgIGZpbHRlcjogYWxwaGEob3BhY2l0eT0wKTtcclxuICAgIG1pbi1oZWlnaHQ6IDEwMCU7XHJcbiAgICBtaW4td2lkdGg6IDEwMCU7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIHRvcDogMDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"back\">\n\t<a href=\"/\">< Back</a>\n</div>\n<hr/>\n<div *ngIf=\"admin\">\n<div class=\"container\">\n        <div class=\"table-wrapper\">\n            <div class=\"table-title\">\n                <div class=\"row\">\n                    <div class=\"col-sm-6\">\n\t\t\t\t\t\t<h2>Manage <b>Products</b></h2>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<a  href='#addModal' class=\"btn btn-success\" data-toggle=\"modal\"><i class=\"glyphicon glyphicon-plus-sign\"></i> <span>Add New Product</span></a>\t\t\t\t\t\t\n\t\t\t\t\t</div>\n                </div>\n            </div>\n            <table class=\"table table-striped table-hover\">\n                <thead>\n                    <tr>\n                        <th>Name</th>\n                        <th>Price</th>\n\t\t\t\t\t\t<th>Details</th>\n                        <th>Category</th>\n                        <th>Stock</th>\n                        <th>Actions</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let list of allProducts\">\n                        <td>{{list.Name}}</td>\n                        <td>{{list.Price | currency}}</td>\n\t\t\t\t\t\t<td>{{list.Details}}</td>\n                        <td>{{list.Category}}</td>\n                        <td>{{list.Stock}}</td>\n                        <td>\n                            <a  class=\"edit\" data-toggle=\"modal\"><i class=\"glyphicon glyphicon-edit\" data-toggle=\"tooltip\" title=\"Edit\" (click)=\"getpopupEdit(list)\"></i></a>\n\t\t\t\t\t\t\t<a  class=\"delete\" data-toggle=\"modal\"><i class=\"glyphicon glyphicon-remove-circle\" data-toggle=\"tooltip\" title=\"Delete\" (click)=\"getpopupRemove(list)\"></i></a>\n\t\t\t\t\t\t</td>\n                    </tr>\n                </tbody>\n\t\t\t</table>\n\n        </div>\n\t</div>\n</div>\n\t\n\t<!-- Add Modal HTML -->\n\t\n<div id=\"addModal\" class=\"modal fade\">\n\t\t<div class=\"modal-dialog\">\n\t\t  <div class=\"modal-content\">\n\t\t\t<form action=\"{{serverUrl}}/Products/add\" method=\"POST\" [formGroup]=\"formGroup\" (validSubmit)=\"onSubmit()\">\n\t\t\t  <div class=\"modal-header\">\t\t\t\t\t\t\n\t\t\t\t<h4 class=\"modal-title\">Add Product</h4>\n\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n\t\t\t  </div>\n\t\t\t  <div class=\"modal-body\">\t\t\t\t\t\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"control-label\">Name</label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"Name\" formControlName=\"Name\" required>\n\t\t\t\t\t\t<span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"control-label\">Price</label>\n\t\t\t\t\t\t<input type=\"number\" min=\"0\" class=\"form-control\" name=\"Price\" formControlName=\"Price\" required>\n\t\t\t\t\t\t<span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"control-label\">Details</label>\n\t\t\t\t\t\t<textarea class=\"form-control\" name=\"Details\" formControlName=\"Details\" required></textarea>\n\t\t\t\t\t\t<span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<!-- <div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"control-label\">ImageUrl</label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"ImageUrl\" formControlName=\"ImageUrl\" required>\n\t\t\t\t\t\t<span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n\t\t\t\t\t</div> -->\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"control-label\">Stock</label>\n\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" min=\"0\" name=\"Stock\" formControlName=\"Stock\" required>\n\t\t\t\t\t\t<span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label>Category</label>\n\t\t\t\t\t\t\t<select class=\"form-control\" name=\"Category\" formControlName=\"Category\">\n\t\t\t\t\t\t\t\t\t<option value=\"electronics\">Electronics</option>\n\t\t\t\t\t\t\t\t\t<option value=\"home\">Home</option>\n\t\t\t\t\t\t\t\t\t<option value=\"accesories\">Accesories</option>\n\t\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\t\n\t\t\t\t\t\n\t\t\t\t\t\t<!-- Imagen upload -->\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"btn btn-success form-control\">\n\t\t\t\t\t\t\t\t<label class=\"fileContainer\"> {{nameFile}}\n\t\t\t\t\t\t\t\t<input type=\"file\" id=\"validateCustomFile\" accept=\"image/*\" (change)=\"selectedFile($event)\">\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<button class=\"btn btn-success form-control\" [disabled]=\"!selectedFiles\" (click)=\"upload()\">Upload</button>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"progress\">\n\t\t\t\t\t\t<div class=\"progress-bar progress-bar-success progress-bar-striped\" role=\"progressbar\" attr.arial-valuenow=\"test\" arial-valuemn=\"0\" arial-valuemax=\"100\" [ngStyle]=\"{width:progress.percentage+'%'}\">\n\t\t\t\t\t\t\t{{progress.percentage}}%\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"control-label\">ImageUrl</label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"ImageUrl\" formControlName=\"ImageUrl\" value=\"{{urlDocument}}\">\n\t\t\t\t</div>\n\n\t\t\t</div>\n\n\t\t\t<div class=\"modal-footer\">\n\t\t\t<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">\n\t\t\t<input type=\"submit\" class=\"btn btn-success\" value=\"Add\">\n\t\t\t</div>\n\t\t\t\n\t\t</form>\n\t\t</div>\n\t</div>\n\t</div>\n\t\t\n\t\t\n\t  \n\t<app-edit [editList] = \"selectedEditProduct\"></app-edit>\n\t<app-remove [removeList] = \"selectedRemoveProduct\"></app-remove>\n\t"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _enviroment_variables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





let AdminComponent = class AdminComponent {
    constructor(http) {
        this.http = http;
        this.allProducts = [];
        this.selectedEditProduct = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"];
        this.selectedRemoveProduct = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"];
        this.addModal = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"];
        this.serverUrl = _enviroment_variables__WEBPACK_IMPORTED_MODULE_3__["ServerUrl"];
        this.nameFile = 'Chose image';
        this.progress = { percentage: 0 };
        this.urlDocumentController = '/product';
        this.admin = false;
    }
    ngOnInit() {
        this.getAllProducts();
        this.validator();
        this.isAdmin();
    }
    getpopupEdit(det) {
        this.selectedEditProduct.next(det);
    }
    getpopupRemove(det) {
        this.selectedRemoveProduct.next(det);
    }
    getpopupAdd(det) {
        this.addModal.next(det);
    }
    getAllProducts() {
        this.http.get(this.serverUrl + '/Products/all').subscribe(data => this.allProducts = data);
    }
    selectedFile(event) {
        const file = event.target.files.item(0);
        if (file.type.match("image.*")) {
            this.selectedFiles = event.target.files;
            this.nameFile = event.target.files[0].name;
        }
        else {
            alert('invalid format!');
        }
    }
    upload() {
        this.progress.percentage = 0;
        this.currentFileUpload = this.selectedFiles.item(0);
        this.pushFile(this.serverUrl + '/Products/uploadFile', this.currentFileUpload).subscribe(data => {
            if (data.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                this.progress.percentage = Math.round(100 * data.loaded / data.total);
            }
            else if (data instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                console.log('succesful');
                this.urlDocument = data.body;
            }
        });
        this.selectedFiles = undefined;
    }
    pushFile(url, file) {
        const formdata = new FormData();
        formdata.append(file.name, file);
        const req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]('POST', url, formdata, {
            reportProgress: true
        });
        return this.http.request(req);
    }
    validator() {
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            Name: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
            ]),
            Price: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^(0|[1-9][0-9]*)$')
                // Validators.minLength(8),
                // Validators.maxLength(20)
            ]),
            Details: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8),
            ]),
            ImageUrl: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [
            // Validators.minLength(8),
            // Validators.maxLength(20)
            ]),
            Stock: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^(0|[1-9][0-9]*)$')
            ]),
            Category: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('electronics', [])
        });
    }
    onSubmit() {
        var json = this.formGroup.value;
        json.ImageUrl = this.urlDocument;
        this.http.post(_enviroment_variables__WEBPACK_IMPORTED_MODULE_3__["ServerUrl"] + '/Products/add', json).subscribe();
        // console.log('Ok');
        location.reload();
    }
    onReset() {
        this.formGroup.reset();
    }
    isAdmin() {
        this.http.get(_enviroment_variables__WEBPACK_IMPORTED_MODULE_3__["ServerUrl"] + '/Customers/isAdmin/').subscribe(data => {
            if (data == 1)
                this.admin = true;
        });
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", Object)
], AdminComponent.prototype, "nameFile", void 0);
AdminComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-admin',
        template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
        styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")]
    }),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], AdminComponent);



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<app-nav></app-nav>\n<div class=\"container\">\n    \n  <router-outlet></router-outlet>\n</div>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

let AppComponent = class AppComponent {
};
AppComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _productlist_productlist_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./productlist/productlist.component */ "./src/app/productlist/productlist.component.ts");
/* harmony import */ var _modals_productdetails_productdetails_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals/productdetails/productdetails.component */ "./src/app/modals/productdetails/productdetails.component.ts");
/* harmony import */ var _modals_edit_edit_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals/edit/edit.component */ "./src/app/modals/edit/edit.component.ts");
/* harmony import */ var _modals_remove_remove_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/remove/remove.component */ "./src/app/modals/remove/remove.component.ts");
/* harmony import */ var _modals_add_add_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modals/add/add.component */ "./src/app/modals/add/add.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./data.service */ "./src/app/data.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _modals_paymodal_payment_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./modals/paymodal/payment.component */ "./src/app/modals/paymodal/payment.component.ts");
/* harmony import */ var _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./thankyou/thankyou.component */ "./src/app/thankyou/thankyou.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _msg_msg_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./msg/msg.component */ "./src/app/msg/msg.component.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _inforproduct_infoproduct_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./inforproduct/infoproduct.component */ "./src/app/inforproduct/infoproduct.component.ts");
/* harmony import */ var _cookiemanager__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./cookiemanager */ "./src/app/cookiemanager.ts");
/* harmony import */ var ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ng-bootstrap-form-validation */ "./node_modules/ng-bootstrap-form-validation/fesm2015/ng-bootstrap-form-validation.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _custom_errors__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./custom-errors */ "./src/app/custom-errors.ts");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _ioservices__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./ioservices */ "./src/app/ioservices.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























let AppModule = class AppModule {
};
AppModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
            _productlist_productlist_component__WEBPACK_IMPORTED_MODULE_3__["ProductlistComponent"],
            _modals_productdetails_productdetails_component__WEBPACK_IMPORTED_MODULE_4__["ProductdetailsComponent"],
            _main_main_component__WEBPACK_IMPORTED_MODULE_8__["MainlistComponent"],
            _nav_nav_component__WEBPACK_IMPORTED_MODULE_9__["NavComponent"],
            _modals_paymodal_payment_component__WEBPACK_IMPORTED_MODULE_12__["PaymentComponent"],
            _modals_edit_edit_component__WEBPACK_IMPORTED_MODULE_5__["EditComponent"],
            _modals_add_add_component__WEBPACK_IMPORTED_MODULE_7__["AddComponent"],
            _modals_remove_remove_component__WEBPACK_IMPORTED_MODULE_6__["RemoveComponent"],
            _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_13__["ThankyouComponent"],
            _search_search_component__WEBPACK_IMPORTED_MODULE_15__["SearchComponent"],
            _admin_admin_component__WEBPACK_IMPORTED_MODULE_17__["AdminComponent"],
            _msg_msg_component__WEBPACK_IMPORTED_MODULE_16__["MsgComponent"],
            _inforproduct_infoproduct_component__WEBPACK_IMPORTED_MODULE_18__["InfoProductComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_14__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_21__["FormsModule"],
            ngx_pagination__WEBPACK_IMPORTED_MODULE_23__["NgxPaginationModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_21__["ReactiveFormsModule"],
            _app_routing__WEBPACK_IMPORTED_MODULE_25__["routing"],
            ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_20__["NgBootstrapFormValidationModule"].forRoot()
        ],
        providers: [_data_service__WEBPACK_IMPORTED_MODULE_10__["DataService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_11__["CookieService"], _ioservices__WEBPACK_IMPORTED_MODULE_24__["IOservices"], _cookiemanager__WEBPACK_IMPORTED_MODULE_19__["CookieManager"], {
                provide: ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_20__["CUSTOM_ERROR_MESSAGES"],
                useValue: _custom_errors__WEBPACK_IMPORTED_MODULE_22__["CUSTOM_ERRORS"],
                multi: true
            }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _productlist_productlist_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./productlist/productlist.component */ "./src/app/productlist/productlist.component.ts");
/* harmony import */ var _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./thankyou/thankyou.component */ "./src/app/thankyou/thankyou.component.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _msg_msg_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./msg/msg.component */ "./src/app/msg/msg.component.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _inforproduct_infoproduct_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./inforproduct/infoproduct.component */ "./src/app/inforproduct/infoproduct.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");








const appRoutes = [
    {
        path: 'cart', component: _productlist_productlist_component__WEBPACK_IMPORTED_MODULE_0__["ProductlistComponent"]
    },
    {
        path: 'thankyou', component: _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_1__["ThankyouComponent"]
    },
    {
        path: 'search', component: _search_search_component__WEBPACK_IMPORTED_MODULE_2__["SearchComponent"]
    },
    {
        path: 'msg', component: _msg_msg_component__WEBPACK_IMPORTED_MODULE_3__["MsgComponent"]
    },
    {
        path: 'DZf1rQHSBkBq4J0qDd54ng', component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_4__["AdminComponent"]
    },
    {
        path: ':id', component: _inforproduct_infoproduct_component__WEBPACK_IMPORTED_MODULE_5__["InfoProductComponent"]
    },
    {
        path: '', component: _main_main_component__WEBPACK_IMPORTED_MODULE_6__["MainlistComponent"]
    }
];
const routing = _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forRoot(appRoutes);


/***/ }),

/***/ "./src/app/cookiemanager.ts":
/*!**********************************!*\
  !*** ./src/app/cookiemanager.ts ***!
  \**********************************/
/*! exports provided: CookieManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieManager", function() { return CookieManager; });
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



let CookieManager = class CookieManager {
    constructor(data, cookieService) {
        this.data = data;
        this.cookieService = cookieService;
        this.cartProducts = [];
        this.totalC = 0;
    }
    ngOnInit() {
    }
    setCookie(totalCart, cartProducts) {
        totalCart += 1;
        //---Send msg to change status Cart
        this.data.changeMessage(totalCart.toString());
        // --- Set info on cookie to update Cart
        this.cookieService.set('info', JSON.stringify({ totalCart: totalCart, items: cartProducts }));
        //console.log(totalCart);
        return totalCart;
    }
};
CookieManager = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_0__["DataService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
], CookieManager);



/***/ }),

/***/ "./src/app/custom-errors.ts":
/*!**********************************!*\
  !*** ./src/app/custom-errors.ts ***!
  \**********************************/
/*! exports provided: CUSTOM_ERRORS, requiredFormat, emailFormat */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOM_ERRORS", function() { return CUSTOM_ERRORS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "requiredFormat", function() { return requiredFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "emailFormat", function() { return emailFormat; });
const CUSTOM_ERRORS = [
    {
        error: "required",
        format: requiredFormat
    }, {
        error: "email",
        format: emailFormat
    }
];
function requiredFormat(label, error) {
    return `${label} is required! * `;
}
function emailFormat(label, error) {
    return `${label} doesn't look like a valid email address.`;
}


/***/ }),

/***/ "./src/app/data.service.ts":
/*!*********************************!*\
  !*** ./src/app/data.service.ts ***!
  \*********************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let DataService = class DataService {
    constructor() {
        this.message = '0';
        this.messageSource = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.message);
        this.currentMessage = this.messageSource.asObservable();
    }
    ngOnInit() {
        this.currentMessage.subscribe(message => this.message = message);
    }
    changeMessage(message) {
        this.messageSource.next(message);
    }
};
DataService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
    __metadata("design:paramtypes", [])
], DataService);



/***/ }),

/***/ "./src/app/enviroment-variables.ts":
/*!*****************************************!*\
  !*** ./src/app/enviroment-variables.ts ***!
  \*****************************************/
/*! exports provided: ServerUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerUrl", function() { return ServerUrl; });
const ServerUrl = "http://localhost:58702";


/***/ }),

/***/ "./src/app/inforproduct/infoproduct.component.css":
/*!********************************************************!*\
  !*** ./src/app/inforproduct/infoproduct.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul > li{margin-right:25px;font-weight:lighter;cursor:pointer}\nli.active{border-bottom:3px solid silver;}\n.item-photo{display:flex;justify-content:center;align-items:center;border-right:1px solid #f6f6f6;}\n.menu-items{list-style-type:none;font-size:11px;display:inline-flex;margin-bottom:0;margin-top:20px}\n.btn-success{width:100%;border-radius:0;}\n.section{width:100%;margin-left:-15px;padding:2px;padding-left:15px;padding-right:15px;background:#f8f9f9}\n.title-price{margin-top:30px;margin-bottom:0;color:black}\n.title-attr{margin-top:0;margin-bottom:0;color:black;}\n.btn-minus{cursor:pointer;font-size:7px;display:flex;align-items:center;padding:5px;padding-left:10px;padding-right:10px;border:1px solid gray;border-radius:2px;border-right:0;}\n.btn-plus{cursor:pointer;font-size:7px;display:flex;align-items:center;padding:5px;padding-left:10px;padding-right:10px;border:1px solid gray;border-radius:2px;border-left:0;}\ndiv.section > div {width:100%;display:inline-flex;}\ndiv.section > div > input {margin:0;padding-left:5px;font-size:10px;padding-right:5px;max-width:18%;text-align:center;}\n.attr,.attr2{cursor:pointer;margin-right:5px;height:20px;font-size:10px;padding:2px;border:1px solid gray;border-radius:2px;}\n.attr.active,.attr2.active{ border:1px solid orange;}\n@media (max-width: 426px) {\n    .container {margin-top:0px !important;}\n    .container > .row{padding:0 !important;}\n    .container > .row > .col-xs-12.col-sm-5{\n        padding-right:0 ;    \n    }\n    .container > .row > .col-xs-12.col-sm-9 > div > p{\n        padding-left:0 !important;\n        padding-right:0 !important;\n    }\n    .container > .row > .col-xs-12.col-sm-9 > div > ul{\n        padding-left:10px !important;\n        \n    }            \n    .section{width:104%;}\n    .menu-items{padding-left:0;}\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5mb3Jwcm9kdWN0L2luZm9wcm9kdWN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsUUFBUSxrQkFBa0Isb0JBQW9CLGNBQWMsQ0FBQztBQUM3RCxVQUFVLCtCQUErQixDQUFDO0FBRTFDLFlBQVksYUFBYSx1QkFBdUIsbUJBQW1CLCtCQUErQixDQUFDO0FBQ25HLFlBQVkscUJBQXFCLGVBQWUsb0JBQW9CLGdCQUFnQixlQUFlLENBQUM7QUFDcEcsYUFBYSxXQUFXLGdCQUFnQixDQUFDO0FBQ3pDLFNBQVMsV0FBVyxrQkFBa0IsWUFBWSxrQkFBa0IsbUJBQW1CLGtCQUFrQixDQUFDO0FBQzFHLGFBQWEsZ0JBQWdCLGdCQUFnQixXQUFXLENBQUM7QUFDekQsWUFBWSxhQUFhLGdCQUFnQixZQUFZLENBQUM7QUFDdEQsV0FBVyxlQUFlLGNBQWMsYUFBYSxtQkFBbUIsWUFBWSxrQkFBa0IsbUJBQW1CLHNCQUFzQixrQkFBa0IsZUFBZSxDQUFDO0FBQ2pMLFVBQVUsZUFBZSxjQUFjLGFBQWEsbUJBQW1CLFlBQVksa0JBQWtCLG1CQUFtQixzQkFBc0Isa0JBQWtCLGNBQWMsQ0FBQztBQUMvSyxtQkFBbUIsV0FBVyxvQkFBb0IsQ0FBQztBQUNuRCwyQkFBMkIsU0FBUyxpQkFBaUIsZUFBZSxrQkFBa0IsY0FBYyxrQkFBa0IsQ0FBQztBQUN2SCxhQUFhLGVBQWUsaUJBQWlCLFlBQVksZUFBZSxZQUFZLHNCQUFzQixrQkFBa0IsQ0FBQztBQUM3SCw0QkFBNEIsd0JBQXdCLENBQUM7QUFFckQ7SUFDSSxZQUFZLDBCQUEwQixDQUFDO0lBQ3ZDLGtCQUFrQixxQkFBcUIsQ0FBQztJQUN4QztRQUNJLGlCQUFpQjtLQUNwQjtJQUNEO1FBQ0ksMEJBQTBCO1FBQzFCLDJCQUEyQjtLQUM5QjtJQUNEO1FBQ0ksNkJBQTZCOztLQUVoQztJQUNELFNBQVMsV0FBVyxDQUFDO0lBQ3JCLFlBQVksZUFBZSxDQUFDO0NBQy9CIiwiZmlsZSI6InNyYy9hcHAvaW5mb3Jwcm9kdWN0L2luZm9wcm9kdWN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ1bCA+IGxpe21hcmdpbi1yaWdodDoyNXB4O2ZvbnQtd2VpZ2h0OmxpZ2h0ZXI7Y3Vyc29yOnBvaW50ZXJ9XG5saS5hY3RpdmV7Ym9yZGVyLWJvdHRvbTozcHggc29saWQgc2lsdmVyO31cblxuLml0ZW0tcGhvdG97ZGlzcGxheTpmbGV4O2p1c3RpZnktY29udGVudDpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyO2JvcmRlci1yaWdodDoxcHggc29saWQgI2Y2ZjZmNjt9XG4ubWVudS1pdGVtc3tsaXN0LXN0eWxlLXR5cGU6bm9uZTtmb250LXNpemU6MTFweDtkaXNwbGF5OmlubGluZS1mbGV4O21hcmdpbi1ib3R0b206MDttYXJnaW4tdG9wOjIwcHh9XG4uYnRuLXN1Y2Nlc3N7d2lkdGg6MTAwJTtib3JkZXItcmFkaXVzOjA7fVxuLnNlY3Rpb257d2lkdGg6MTAwJTttYXJnaW4tbGVmdDotMTVweDtwYWRkaW5nOjJweDtwYWRkaW5nLWxlZnQ6MTVweDtwYWRkaW5nLXJpZ2h0OjE1cHg7YmFja2dyb3VuZDojZjhmOWY5fVxuLnRpdGxlLXByaWNle21hcmdpbi10b3A6MzBweDttYXJnaW4tYm90dG9tOjA7Y29sb3I6YmxhY2t9XG4udGl0bGUtYXR0cnttYXJnaW4tdG9wOjA7bWFyZ2luLWJvdHRvbTowO2NvbG9yOmJsYWNrO31cbi5idG4tbWludXN7Y3Vyc29yOnBvaW50ZXI7Zm9udC1zaXplOjdweDtkaXNwbGF5OmZsZXg7YWxpZ24taXRlbXM6Y2VudGVyO3BhZGRpbmc6NXB4O3BhZGRpbmctbGVmdDoxMHB4O3BhZGRpbmctcmlnaHQ6MTBweDtib3JkZXI6MXB4IHNvbGlkIGdyYXk7Ym9yZGVyLXJhZGl1czoycHg7Ym9yZGVyLXJpZ2h0OjA7fVxuLmJ0bi1wbHVze2N1cnNvcjpwb2ludGVyO2ZvbnQtc2l6ZTo3cHg7ZGlzcGxheTpmbGV4O2FsaWduLWl0ZW1zOmNlbnRlcjtwYWRkaW5nOjVweDtwYWRkaW5nLWxlZnQ6MTBweDtwYWRkaW5nLXJpZ2h0OjEwcHg7Ym9yZGVyOjFweCBzb2xpZCBncmF5O2JvcmRlci1yYWRpdXM6MnB4O2JvcmRlci1sZWZ0OjA7fVxuZGl2LnNlY3Rpb24gPiBkaXYge3dpZHRoOjEwMCU7ZGlzcGxheTppbmxpbmUtZmxleDt9XG5kaXYuc2VjdGlvbiA+IGRpdiA+IGlucHV0IHttYXJnaW46MDtwYWRkaW5nLWxlZnQ6NXB4O2ZvbnQtc2l6ZToxMHB4O3BhZGRpbmctcmlnaHQ6NXB4O21heC13aWR0aDoxOCU7dGV4dC1hbGlnbjpjZW50ZXI7fVxuLmF0dHIsLmF0dHIye2N1cnNvcjpwb2ludGVyO21hcmdpbi1yaWdodDo1cHg7aGVpZ2h0OjIwcHg7Zm9udC1zaXplOjEwcHg7cGFkZGluZzoycHg7Ym9yZGVyOjFweCBzb2xpZCBncmF5O2JvcmRlci1yYWRpdXM6MnB4O31cbi5hdHRyLmFjdGl2ZSwuYXR0cjIuYWN0aXZleyBib3JkZXI6MXB4IHNvbGlkIG9yYW5nZTt9XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA0MjZweCkge1xuICAgIC5jb250YWluZXIge21hcmdpbi10b3A6MHB4ICFpbXBvcnRhbnQ7fVxuICAgIC5jb250YWluZXIgPiAucm93e3BhZGRpbmc6MCAhaW1wb3J0YW50O31cbiAgICAuY29udGFpbmVyID4gLnJvdyA+IC5jb2wteHMtMTIuY29sLXNtLTV7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6MCA7ICAgIFxuICAgIH1cbiAgICAuY29udGFpbmVyID4gLnJvdyA+IC5jb2wteHMtMTIuY29sLXNtLTkgPiBkaXYgPiBwe1xuICAgICAgICBwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OjAgIWltcG9ydGFudDtcbiAgICB9XG4gICAgLmNvbnRhaW5lciA+IC5yb3cgPiAuY29sLXhzLTEyLmNvbC1zbS05ID4gZGl2ID4gdWx7XG4gICAgICAgIHBhZGRpbmctbGVmdDoxMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIFxuICAgIH0gICAgICAgICAgICBcbiAgICAuc2VjdGlvbnt3aWR0aDoxMDQlO31cbiAgICAubWVudS1pdGVtc3twYWRkaW5nLWxlZnQ6MDt9XG59Il19 */"

/***/ }),

/***/ "./src/app/inforproduct/infoproduct.component.html":
/*!*********************************************************!*\
  !*** ./src/app/inforproduct/infoproduct.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"back\">\n    <a href=\"/\">< Back</a>\n  </div>\n  <hr>\n    <div *ngIf=\"product !== undefined\" class=\"container\">\n    <div class=\"row\">\n         <div class=\"col-xs-4 item-photo\">\n            <img src=\"data:image/png;base64,{{product.ImageUrl}}\" class=\"img-thumbnail\" style=\"max-width:100%;\"/>\n          </div>\n          <div class=\"col-xs-5\" style=\"border:0px solid gray\">\n              <!-- Datos del vendedor y titulo del producto -->\n              <h3>{{product.Name}} - {{product.Id}}</h3>    \n              <h5 style=\"color:#337ab7\">Category - <a (click)=\"onSearch(product.Category)\">{{product.Category}}</a></h5>\n  \n              <!-- Precios -->\n              <h6 class=\"title-price\"><small>Price</small></h6>\n              <h3 style=\"margin-top:0px;\">{{product.Price | currency}}</h3>\n  \n              <!-- Detalles especificos del producto -->\n              <div class=\"section\">\n                  <h6 class=\"title-attr\" style=\"margin-top:15px;\" ><small>COLOR</small></h6>                    \n                  <div>\n                      <div class=\"attr\" style=\"width:25px;background:#5a5a5a;\"></div>\n                      <div class=\"attr\" style=\"width:25px;background:white;\"></div>\n                  </div>\n              </div>  \n              <div class=\"section\" style=\"padding-bottom:20px;\">\n                  <h6 class=\"title-attr\"><small>Quantity</small></h6>                    \n                  <div>\n                      <input type=\"number\" max=\"{{totalCanAdd}}\" min = \"1\" value=\"1\" #qty/>\n                      <small> &nbsp; maximum quantity  <b style=\"color:red;\">{{totalCanAdd}}</b>  </small>\n                  </div>\n              </div>                \n  \n              <!-- Botones de compra -->\n              <div *ngIf=\"stock(totalCanAdd)  else notShowInput\">\n                  <button class=\"btn btn-success\" (click)=\"newMessage( $event , qty )\" value=\"{{product.Id}}\"><span style=\"margin-right:20px\" class=\"glyphicon glyphicon-shopping-cart\" aria-hidden=\"true\"></span> Add to Cart</button>\n                </div>\n\n                <ng-template #notShowInput>\n                   <button class=\"btn btn-success\" value=\"{{product.Id}}\" disabled><span style=\"margin-right:20px\" class=\"glyphicon glyphicon-shopping-cart\" aria-hidden=\"true\"></span> Add to Cart</button>\n                </ng-template>\n          </div>                              \n  \n          <div class=\"col-xs-9\">\n              <ul class=\"menu-items\">\n                  <li>Product Details</li>\n              </ul>\n              <div style=\"width:100%;border-top:1px solid silver\">\n                  <p style=\"padding:15px;\">\n                      <small>\n                        {{product.Details}}\n                      </small>\n                  </p>\n                \n              </div>\n          </div>\t\t\n      </div>\n  </div>      \n  \n  <script>\n   $(document).ready(function(){\n            //-- Click on detail\n            $(\"ul.menu-items > li\").on(\"click\",function(){\n                $(\"ul.menu-items > li\").removeClass(\"active\");\n                $(this).addClass(\"active\");\n            })\n\n            $(\".attr,.attr2\").on(\"click\",function(){\n                var clase = $(this).attr(\"class\");\n\n                $(\".\" + clase).removeClass(\"active\");\n                $(this).addClass(\"active\");\n            })\n\n            //-- Click on QUANTITY\n            $(\".btn-minus\").on(\"click\",function(){\n                var now = $(\".section > div > input\").val();\n                if ($.isNumeric(now)){\n                    if (parseInt(now) -1 > 0){ now--;}\n                    $(\".section > div > input\").val(now);\n                }else{\n                    $(\".section > div > input\").val(\"1\");\n                }\n            })            \n            $(\".btn-plus\").on(\"click\",function(){\n                var now = $(\".section > div > input\").val();\n                if ($.isNumeric(now)){\n                    $(\".section > div > input\").val(parseInt(now)+1);\n                }else{\n                    $(\".section > div > input\").val(\"1\");\n                }\n            })                        \n        }) </script>\n\n"

/***/ }),

/***/ "./src/app/inforproduct/infoproduct.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/inforproduct/infoproduct.component.ts ***!
  \*******************************************************/
/*! exports provided: InfoProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoProductComponent", function() { return InfoProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _enviroment_variables__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _cookiemanager__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../cookiemanager */ "./src/app/cookiemanager.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







let InfoProductComponent = class InfoProductComponent {
    constructor(data, http, route, router, cookieService, cookieManager) {
        this.data = data;
        this.http = http;
        this.route = route;
        this.router = router;
        this.cookieService = cookieService;
        this.cookieManager = cookieManager;
        this.serverUrl = _enviroment_variables__WEBPACK_IMPORTED_MODULE_2__["ServerUrl"];
        this.totalCart = 0;
        this.cartProducts = [];
        this.allProducts = [];
        this.realProducts = [];
        this.totalCanAdd = 0;
        this.productId = 0;
        this.getAllProducts();
        this.getAllRealProducts();
    }
    ngOnInit() {
        if (localStorage.getItem('info') != null) {
            var info = JSON.parse(localStorage.getItem('info'));
            this.totalCart = info.totalCart;
        }
        this.getAllProducts();
        this.getAllRealProducts();
        this.getDetails();
    }
    getDetails() {
        this.route.params.subscribe(params => {
            this.productId = params['id'] !== undefined ? +params['id'] : 0;
            if (this.productId !== 0) {
                this.http.get(this.serverUrl + '/Products/findById/' + this.productId).subscribe(data => {
                    this.product = data;
                    this.totalCanAdd = this.product.Stock;
                    var cart = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
                    console.log(cart);
                    cart.forEach(element => { if (element.Id == this.product.Id)
                        this.totalCanAdd = this.product.Stock - element.Stock; });
                });
            }
        });
    }
    // onSearch( categoy: any ) {
    //     this.http.get<IProducts[]>( ServerUrl + '/Products/search/' + categoy + '/none').subscribe(  data => {
    //       data.forEach( element => element.Stock = ( element.Stock > 0 ) ? 1 : 0 );
    //       ( data.length == 0 ) ? this.cookieService.delete( 'search' ) :  this.cookieService.set( 'search', JSON.stringify( { items : data } ));
    //       this.router.navigate(['/search']);
    //       location.reload();
    //     });
    //   }
    newMessage(event, qty) {
        this.add(event.target.value, qty.value);
        location.reload();
    }
    add(pid, qty) {
        var flag = false;
        //---Validate if Json on cookie is empty, in that case work with this.cartProduct list
        this.cartProducts = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
        this.allProducts.forEach(element => {
            element.Stock = +qty;
            if (element.Id == pid) {
                //---If cartProducts is empty then put element inside list on else
                if (this.cartProducts.length != 0) {
                    for (let cartProd of this.cartProducts) {
                        // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
                        if (cartProd.Id == element.Id) {
                            var product = this.productById(element.Id);
                            cartProd.Stock += +qty;
                            cartProd.Quantity = product.Stock - cartProd.Stock;
                            cartProd.RealQty = product.Stock;
                            flag = false;
                            break;
                        }
                        else {
                            flag = true;
                        }
                    }
                    if (flag)
                        this.addCart(element);
                }
                else {
                    this.addCart(element);
                }
            }
        });
        this.totalCart += +qty;
        //---Send msg to change status Cart
        this.data.changeMessage(this.totalCart.toString());
        // --- Set info on cookie to update Cart
        localStorage.setItem('info', JSON.stringify({ totalCart: this.totalCart, items: this.cartProducts }));
    }
    addCart(product) {
        var prod = this.productById(product.Id);
        product.Quantity = prod.Stock - product.Stock;
        product.RealQty = prod.Stock;
        this.cartProducts.push(product);
    }
    productById(id) {
        var product;
        this.realProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
    cartProductById(id) {
        var product;
        this.cartProducts = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
        this.cartProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
    getAllProducts() {
        this.http.get(this.serverUrl + '/Products/productImages').subscribe(data => {
            this.allProducts = data;
            this.allProducts.forEach(element => element.Stock = (element.Stock > 0) ? 1 : 0);
        });
    }
    getAllRealProducts() {
        this.http.get(this.serverUrl + '/Products/all').subscribe(data => {
            this.realProducts = data;
        });
    }
    stock(number) {
        return (number > 0) ? true : false;
    }
};
InfoProductComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-infoproduct',
        template: __webpack_require__(/*! ./infoproduct.component.html */ "./src/app/inforproduct/infoproduct.component.html"),
        styles: [__webpack_require__(/*! ./infoproduct.component.css */ "./src/app/inforproduct/infoproduct.component.css")]
    }),
    __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"], _cookiemanager__WEBPACK_IMPORTED_MODULE_6__["CookieManager"]])
], InfoProductComponent);



/***/ }),

/***/ "./src/app/ioservices.ts":
/*!*******************************!*\
  !*** ./src/app/ioservices.ts ***!
  \*******************************/
/*! exports provided: IOservices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IOservices", function() { return IOservices; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let IOservices = class IOservices {
    constructor() {
        this.inCart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
    }
    sendMessage() {
        this.inCart.emit("inCart");
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
    __metadata("design:type", Object)
], IOservices.prototype, "inCart", void 0);
IOservices = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
    __metadata("design:paramtypes", [])
], IOservices);



/***/ }),

/***/ "./src/app/main/main.component.css":
/*!*****************************************!*\
  !*** ./src/app/main/main.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".div-images {\n    height: 350px; width: 350px;\n    margin:0 auto;\n    \n}\n\nimg {\n    border: 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9tYWluLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjLENBQUMsYUFBYTtJQUM1QixjQUFjOztDQUVqQjs7QUFFRDtJQUNJLFVBQVU7Q0FDYiIsImZpbGUiOiJzcmMvYXBwL21haW4vbWFpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdi1pbWFnZXMge1xuICAgIGhlaWdodDogMzUwcHg7IHdpZHRoOiAzNTBweDtcbiAgICBtYXJnaW46MCBhdXRvO1xuICAgIFxufVxuXG5pbWcge1xuICAgIGJvcmRlcjogMDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/main/main.component.html":
/*!******************************************!*\
  !*** ./src/app/main/main.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "      <div class=\"row\">\n          <div class=\"col-sm-6\" *ngFor=\"let list of allProducts  | paginate: { itemsPerPage: itemsPerPage, currentPage: p }\">\n              <div *ngIf=\"list !== undefined\" class=\"card\" style=\"width: 50rem;\">\n                <div class=\"div-images\">\n                  <img  src=\"data:image/png;base64,{{list.ImageUrl}}\" class=\"img-thumbnail\" />\n                </div>\n                  <div class=\"card-body\">\n                    <h5 class=\"card-title\"><h4>{{list.Name}} - {{list.Id}} ( {{list.Category}} )</h4></h5>\n                    <p class=\"card-text\">{{list.Price | currency}}</p>\n                    <div *ngIf=\"stock(list.Id)  else notShow\">\n                      <p class=\"card-text\" style=\"color: green;\"> Available <i class=\"fa fa-check-circle-o\" aria-hidden=\"true\"></i></p>\n                    </div>\n\n                    <ng-template #notShow>\n                      <p class=\"card-text\" style=\"color: red;\">Unavailable <i class=\"fa fa-times-circle-o\" aria-hidden=\"true\"></i></p>\n                    </ng-template>\n                    \n                    <button class=\"btn btn-primary btn-sm form-control\"  routerLink=\"/{{list.Id}}\">View info</button>\n                    <br><br>\n\n                    \n\n                    <div *ngIf=\"stock(list.Id)  else notShowInput\">\n                      <button class=\"btn btn-success btn-sm form-control\" (click)=\"newMessage($event)\" value=\"{{list.Id}}\">Add to Cart</button>\n                    </div>\n\n                    <ng-template #notShowInput>\n                      <button class=\"btn btn-success btn-sm form-control\" value=\"{{list.Id}}\" disabled>Add to Cart</button>\n                    </ng-template>\n\n                    <hr> <br> <br>\n                  </div>\n                </div>\n          </div>\n      </div>\n\n      <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\n\n\n  <app-productdetails [list] = \"selectedProduct\"></app-productdetails>\n\n"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainlistComponent", function() { return MainlistComponent; });
/* harmony import */ var _cookiemanager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../cookiemanager */ "./src/app/cookiemanager.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _enviroment_variables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








let MainlistComponent = class MainlistComponent {
    constructor(data, cookieService, http, router, cookieManager) {
        this.data = data;
        this.cookieService = cookieService;
        this.http = http;
        this.router = router;
        this.cookieManager = cookieManager;
        this.totalCart = 0;
        this.cartProducts = [];
        this.selectedProduct = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"];
        this.allProducts = [];
        this.realProducts = [];
        this.serverUrl = _enviroment_variables__WEBPACK_IMPORTED_MODULE_6__["ServerUrl"];
        this.itemsPerPage = 5;
        this.getAllProducts();
        this.getAllRealProducts();
    }
    ngOnInit() {
        if (localStorage.getItem('info') != null) {
            const info = JSON.parse(localStorage.getItem('info'));
            this.totalCart = info.totalCart;
        }
        this.getAllProducts();
        this.getAllRealProducts();
        this.itemsPerPage = parseInt(this.cookieService.get("itemsPerPage"));
        console.log(this.itemsPerPage);
    }
    newMessage(event) {
        this.add(event.target.value);
    }
    getpopup(det) {
        this.selectedProduct.next(det);
    }
    getDetails(id) {
        this.router.navigate(['/info', { id: id }]);
    }
    add(pid) {
        let flag = false;
        // ---Validate if Json on cookie is empty, in that case work with this.cartProduct list
        this.cartProducts = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
        this.allProducts.forEach(element => {
            if (element.Id == pid) {
                // ---If cartProducts is empty then put element inside list on else
                if (this.cartProducts.length != 0) {
                    for (let cartProd of this.cartProducts) {
                        // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
                        if (cartProd.Id == element.Id) {
                            var product = this.productById(element.Id);
                            cartProd.Stock += 1;
                            cartProd.Quantity = product.Stock - cartProd.Stock;
                            cartProd.RealQty = product.Stock;
                            flag = false;
                            break;
                        }
                        else {
                            flag = true;
                        }
                    }
                    if (flag) {
                        this.addCart(element);
                    }
                }
                else {
                    this.addCart(element);
                }
            }
        });
        this.totalCart += 1;
        //---Send msg to change status Cart
        this.data.changeMessage(this.totalCart.toString());
        // --- Set info on cookie to update Cart
        localStorage.setItem('info', JSON.stringify({ totalCart: this.totalCart, items: this.cartProducts }));
    }
    addCart(product) {
        var prod = this.productById(product.Id);
        product.Quantity = prod.Stock - product.Stock;
        product.RealQty = prod.Stock;
        this.cartProducts.push(product);
    }
    getAllProducts() {
        this.http.get(this.serverUrl + '/Products/productImages').subscribe(data => {
            this.allProducts = data;
            this.allProducts.forEach(element => element.Stock = (element.Stock > 0) ? 1 : 0);
        });
    }
    getAllRealProducts() {
        this.http.get(this.serverUrl + '/Products/all').subscribe(data => {
            this.realProducts = data;
        });
    }
    stock(id) {
        var cartProduct;
        this.cartProducts = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
        this.allProducts.forEach(element => {
            if (element.Id == id) {
                // ---If cartProducts is empty then put element inside list on else
                if (this.cartProducts.length != 0) {
                    for (let cartProd of this.cartProducts) {
                        // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
                        if (cartProd.Id == element.Id) {
                            var product = this.productById(element.Id);
                            if (product != undefined) {
                                //cartProd.Stock += 1;
                                cartProd.Quantity = product.Stock - cartProd.Stock;
                                cartProd.RealQty = product.Stock;
                                cartProduct = cartProd;
                                break;
                            }
                        }
                    }
                }
            }
        });
        var qty;
        if (cartProduct != undefined) {
            qty = cartProduct.Quantity;
        }
        else {
            var aux = this.cartProductById(id);
            if (aux != undefined)
                qty = aux.Stock;
        }
        return (qty > 0) ? true : false;
    }
    productById(id) {
        var product;
        this.realProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
    cartProductById(id) {
        var product;
        this.allProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
};
MainlistComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-mainlist',
        template: __webpack_require__(/*! ./main.component.html */ "./src/app/main/main.component.html"),
        styles: [__webpack_require__(/*! ./main.component.css */ "./src/app/main/main.component.css")]
    }),
    __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], _cookiemanager__WEBPACK_IMPORTED_MODULE_0__["CookieManager"]])
], MainlistComponent);



/***/ }),

/***/ "./src/app/modals/add/add.component.css":
/*!**********************************************!*\
  !*** ./src/app/modals/add/add.component.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9hZGQvYWRkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modals/add/add.component.html":
/*!***********************************************!*\
  !*** ./src/app/modals/add/add.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Add Modal HTML -->\n<div id=\"addModal\" class=\"modal fade\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <form>\n        <div class=\"modal-header\">\t\t\t\t\t\t\n          <h4 class=\"modal-title\">Add Product</h4>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        </div>\n        <div class=\"modal-body\">\t\t\t\t\t\n          <div class=\"form-group\">\n            <label>Name</label>\n            <input type=\"text\" class=\"form-control\" required>\n          </div>\n          <div class=\"form-group\">\n            <label>Email</label>\n            <input type=\"email\" class=\"form-control\" required>\n          </div>\n          <div class=\"form-group\">\n            <label>Address</label>\n            <textarea class=\"form-control\" required></textarea>\n          </div>\n          <div class=\"form-group\">\n            <label>Phone</label>\n            <input type=\"text\" class=\"form-control\" required>\n          </div>\t\t\t\t\t\n        </div>\n        <div class=\"modal-footer\">\n          <input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">\n          <input type=\"submit\" class=\"btn btn-success\" value=\"Add\">\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/add/add.component.ts":
/*!*********************************************!*\
  !*** ./src/app/modals/add/add.component.ts ***!
  \*********************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



let AddComponent = class AddComponent {
    constructor(http) {
        this.http = http;
    }
    ngOnInit() {
        this.modal.subscribe(det => {
            this.fulldetails = det;
            $("#addModal").modal('show');
        }, error => {
            console.log(error);
        });
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"])
], AddComponent.prototype, "modal", void 0);
AddComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-add',
        template: __webpack_require__(/*! ./add.component.html */ "./src/app/modals/add/add.component.html"),
        styles: [__webpack_require__(/*! ./add.component.css */ "./src/app/modals/add/add.component.css")]
    }),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], AddComponent);



/***/ }),

/***/ "./src/app/modals/edit/edit.component.css":
/*!************************************************!*\
  !*** ./src/app/modals/edit/edit.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fileContainer {\r\n    overflow: hidden;\r\n    position: relative;\r\n}\r\n\r\n.fileContainer [type=file] {\r\n    cursor: inherit;\r\n    display: block;\r\n    font-size: 999px;\r\n    filter: alpha(opacity=0);\r\n    min-height: 100%;\r\n    min-width: 100%;\r\n    opacity: 0;\r\n    position: absolute;\r\n    right: 0;\r\n    text-align: right;\r\n    top: 0;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kYWxzL2VkaXQvZWRpdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0lBQ2pCLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsU0FBUztJQUNULGtCQUFrQjtJQUNsQixPQUFPO0NBQ1YiLCJmaWxlIjoic3JjL2FwcC9tb2RhbHMvZWRpdC9lZGl0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmlsZUNvbnRhaW5lciB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4uZmlsZUNvbnRhaW5lciBbdHlwZT1maWxlXSB7XHJcbiAgICBjdXJzb3I6IGluaGVyaXQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtc2l6ZTogOTk5cHg7XHJcbiAgICBmaWx0ZXI6IGFscGhhKG9wYWNpdHk9MCk7XHJcbiAgICBtaW4taGVpZ2h0OiAxMDAlO1xyXG4gICAgbWluLXdpZHRoOiAxMDAlO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICB0b3A6IDA7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modals/edit/edit.component.html":
/*!*************************************************!*\
  !*** ./src/app/modals/edit/edit.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!-- Edit Modal HTML -->\n\t<div id=\"editModal\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div *ngIf=\"editdetails !== undefined\">\n                <form action=\"{{serverUrl}}/Products/edit\" method=\"POST\" [formGroup]=\"formGroup\" (validSubmit)=\"onSubmit(editdetails.Id)\">\n                  <div class=\"modal-header\">\t\t\t\t\t\t\n                    <h4 class=\"modal-title\">Edit Product</h4>\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n                  </div>\n                  <div class=\"modal-body\">\t\t\t\n                    <div class=\"form-group\" hidden>\n                        <label>Id</label>\n                        <input type=\"text\" class=\"form-control\" value=\"{{editdetails.Id}}\" name=\"Id\" formControlName=\"Id\" >\n                      </div>\t\t\n                    <div class=\"form-group\">\n                      <label class=\"control-label\">Name</label>\n                      <input type=\"text\" class=\"form-control\" value=\"{{editdetails.Name}}\" name=\"Name\" formControlName=\"Name\" required>\n                      <span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n                    </div>\n                    <div class=\"form-group\">\n                      <label class=\"control-label\">Price</label>\n                      <input type=\"number\" min=\"0\" class=\"form-control\" value=\"{{editdetails.Price}}\" name=\"Price\" formControlName=\"Price\" required>\n                      <span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n                    </div>\n                    <div class=\"form-group\">\n                      <label class=\"control-label\">Details</label>\n                      <textarea class=\"form-control\" value=\"{{editdetails.Details}}\" name=\"Details\" formControlName=\"Details\" required></textarea>\n                      <span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n                    </div>\n                    \t\n                    <div class=\"form-group\">\n                        <label class=\"control-label\">Stock</label>\n                        <input type=\"number\" min=\"0\" class=\"form-control\" value=\"{{editdetails.Stock}}\" name=\"Stock\" formControlName=\"Stock\"  required>\n                        <span style=\"color: red;\">\t<bfv-messages></bfv-messages></span>\n                    </div>\t\n                   \n                      <div class=\"form-group\">\n                        <label>Category</label>\n                        <select class=\"form-control\" name=\"Category\" value=\"{{editdetails.Category}}\" formControlName=\"Category\" required>\n                            <option value=\"electronics\">Electronics</option>\n                            <option value=\"home\">Home</option>\n                            <option value=\"accesories\">Accesories</option>\n                        </select>\n                    </div>\n\n                   \n                  \t<!-- Imagen upload -->\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"btn btn-success form-control\">\n\t\t\t\t\t\t\t\t<label class=\"fileContainer\"> {{nameFile}}\n\t\t\t\t\t\t\t\t<input type=\"file\" id=\"validateCustomFile\" accept=\"image/*\" (change)=\"selectedFile($event)\">\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<button class=\"btn btn-success form-control\" [disabled]=\"!selectedFiles\" (click)=\"upload()\">Upload</button>\n\t\t\t\t</div>\n\n        <!-- Progress bar -->\n\t\t\t\t<div class=\"progress\">\n\t\t\t\t\t\t<div class=\"progress-bar progress-bar-success progress-bar-striped\" role=\"progressbar\" attr.arial-valuenow=\"test\" arial-valuemn=\"0\" arial-valuemax=\"100\" [ngStyle]=\"{width:progress.percentage+'%'}\">\n\t\t\t\t\t\t\t{{progress.percentage}}%\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<!-- Input with the name of image -->\n        <div class=\"form-group\">\n            <label class=\"control-label\">ImageUrl</label>\n            <input type=\"text\" class=\"form-control\" name=\"ImageUrl\" value=\"{{editdetails.ImageUrl}}\" formControlName=\"ImageUrl\" required>\n        </div>\n\n        \n                  </div>\n                  <div class=\"modal-footer\">\n                    <input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">\n                    <input type=\"submit\" class=\"btn btn-info\" value=\"Save\">\n                  </div>\n                </form>\n              </div>\n         \n        </div>\n      </div>\n    </div>"

/***/ }),

/***/ "./src/app/modals/edit/edit.component.ts":
/*!***********************************************!*\
  !*** ./src/app/modals/edit/edit.component.ts ***!
  \***********************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





let EditComponent = class EditComponent {
    constructor(http) {
        this.http = http;
        this.serverUrl = src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_2__["ServerUrl"];
        this.nameFile = 'Chose image';
        this.progress = { percentage: 0 };
        this.urlDocumentController = '/product';
        this.getAllRealProducts();
    }
    ngOnInit() {
        this.getAllRealProducts();
        this.editList.subscribe(det => {
            this.editdetails = det;
            this.validator(det.Id);
            this.selectedCategory = det.Category;
            $("#editModal").modal('show');
        }, error => {
            console.log(error);
        });
    }
    validator(id) {
        var product = this.productById(id);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            Id: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.Id, [
            //Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ]),
            Name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.Name, [
            //Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ]),
            Price: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.Price, [
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1)
                // Validators.minLength(8),
                // Validators.maxLength(20)
            ]),
            Details: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.Details, [
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8),
            ]),
            Stock: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.Stock, [
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1)
            ]),
            Category: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.Category, []),
            ImageUrl: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](product.ImageUrl, [
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1)
                // Validators.minLength(8),
                // Validators.maxLength(20)
            ])
        });
    }
    onSubmit() {
        console.log(this.formGroup);
        var json = this.formGroup.value;
        this.http.post(src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_2__["ServerUrl"] + '/Products/edit', json).subscribe();
        console.log('Ok');
        location.reload();
    }
    onReset() {
        this.formGroup.reset();
    }
    selectedFile(event) {
        const file = event.target.files.item(0);
        if (file.type.match("image.*")) {
            this.selectedFiles = event.target.files;
            this.nameFile = event.target.files[0].name;
        }
        else {
            alert('invalid format!');
        }
    }
    upload() {
        this.progress.percentage = 0;
        this.currentFileUpload = this.selectedFiles.item(0);
        this.pushFile(this.serverUrl + '/Products/uploadFile', this.currentFileUpload).subscribe(data => {
            if (data.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpEventType"].UploadProgress) {
                this.progress.percentage = Math.round(100 * data.loaded / data.total);
            }
            else if (data instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpResponse"]) {
                console.log('succesful');
                this.urlDocument = data.body;
            }
        });
        this.selectedFiles = undefined;
    }
    pushFile(url, file) {
        const formdata = new FormData();
        formdata.append(file.name, file);
        const req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpRequest"]('POST', url, formdata, {
            reportProgress: true
        });
        return this.http.request(req);
    }
    getAllRealProducts() {
        this.http.get(this.serverUrl + '/Products/all').subscribe(data => {
            this.realProducts = data;
        });
    }
    productById(id) {
        var product;
        this.realProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"])
], EditComponent.prototype, "editList", void 0);
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", Object)
], EditComponent.prototype, "nameFile", void 0);
EditComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-edit',
        template: __webpack_require__(/*! ./edit.component.html */ "./src/app/modals/edit/edit.component.html"),
        styles: [__webpack_require__(/*! ./edit.component.css */ "./src/app/modals/edit/edit.component.css")]
    }),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
], EditComponent);



/***/ }),

/***/ "./src/app/modals/paymodal/payment.component.css":
/*!*******************************************************!*\
  !*** ./src/app/modals/paymodal/payment.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body { margin:50px auto; width:600px;}\n\n/* CSS for Credit Card Payment form */\n\n.credit-card-box .panel-title {\n    display: inline;\n    font-weight: bold;\n}\n\n.credit-card-box .form-control.error {\n    border-color: red;\n    outline: 0;\n    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(255,0,0,0.6);\n}\n\n.credit-card-box label.error {\n  font-weight: bold;\n  color: red;\n  padding: 2px 8px;\n  margin-top: 2px;\n}\n\n.credit-card-box .payment-errors {\n  font-weight: bold;\n  color: red;\n  padding: 2px 8px;\n  margin-top: 2px;\n}\n\n.credit-card-box label {\n    display: block;\n}\n\n/* The old \"center div vertically\" hack */\n\n.credit-card-box .display-table {\n    display: table;\n}\n\n.credit-card-box .display-tr {\n    display: table-row;\n}\n\n.credit-card-box .display-td {\n    display: table-cell;\n    vertical-align: middle;\n    width: 100%;\n}\n\n/* Just looks nicer */\n\n.credit-card-box .panel-heading img {\n    min-width: 180px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kYWxzL3BheW1vZGFsL3BheW1lbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLGlCQUFpQixDQUFDLFlBQVksQ0FBQzs7QUFFdEMsc0NBQXNDOztBQUN0QztJQUNJLGdCQUFnQjtJQUNoQixrQkFBa0I7Q0FDckI7O0FBQ0Q7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLHdFQUF3RTtDQUMzRTs7QUFDRDtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLGdCQUFnQjtDQUNqQjs7QUFDRDtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLGdCQUFnQjtDQUNqQjs7QUFDRDtJQUNJLGVBQWU7Q0FDbEI7O0FBQ0QsMENBQTBDOztBQUMxQztJQUNJLGVBQWU7Q0FDbEI7O0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7O0FBQ0Q7SUFDSSxvQkFBb0I7SUFDcEIsdUJBQXVCO0lBQ3ZCLFlBQVk7Q0FDZjs7QUFDRCxzQkFBc0I7O0FBQ3RCO0lBQ0ksaUJBQWlCO0NBQ3BCIiwiZmlsZSI6InNyYy9hcHAvbW9kYWxzL3BheW1vZGFsL3BheW1lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHkgeyBtYXJnaW46NTBweCBhdXRvOyB3aWR0aDo2MDBweDt9XG5cbi8qIENTUyBmb3IgQ3JlZGl0IENhcmQgUGF5bWVudCBmb3JtICovXG4uY3JlZGl0LWNhcmQtYm94IC5wYW5lbC10aXRsZSB7XG4gICAgZGlzcGxheTogaW5saW5lO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNyZWRpdC1jYXJkLWJveCAuZm9ybS1jb250cm9sLmVycm9yIHtcbiAgICBib3JkZXItY29sb3I6IHJlZDtcbiAgICBvdXRsaW5lOiAwO1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDAgMXB4IDFweCByZ2JhKDAsMCwwLDAuMDc1KSwwIDAgOHB4IHJnYmEoMjU1LDAsMCwwLjYpO1xufVxuLmNyZWRpdC1jYXJkLWJveCBsYWJlbC5lcnJvciB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogcmVkO1xuICBwYWRkaW5nOiAycHggOHB4O1xuICBtYXJnaW4tdG9wOiAycHg7XG59XG4uY3JlZGl0LWNhcmQtYm94IC5wYXltZW50LWVycm9ycyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogcmVkO1xuICBwYWRkaW5nOiAycHggOHB4O1xuICBtYXJnaW4tdG9wOiAycHg7XG59XG4uY3JlZGl0LWNhcmQtYm94IGxhYmVsIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cbi8qIFRoZSBvbGQgXCJjZW50ZXIgZGl2IHZlcnRpY2FsbHlcIiBoYWNrICovXG4uY3JlZGl0LWNhcmQtYm94IC5kaXNwbGF5LXRhYmxlIHtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbn1cbi5jcmVkaXQtY2FyZC1ib3ggLmRpc3BsYXktdHIge1xuICAgIGRpc3BsYXk6IHRhYmxlLXJvdztcbn1cbi5jcmVkaXQtY2FyZC1ib3ggLmRpc3BsYXktdGQge1xuICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi8qIEp1c3QgbG9va3MgbmljZXIgKi9cbi5jcmVkaXQtY2FyZC1ib3ggLnBhbmVsLWhlYWRpbmcgaW1nIHtcbiAgICBtaW4td2lkdGg6IDE4MHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/modals/paymodal/payment.component.html":
/*!********************************************************!*\
  !*** ./src/app/modals/paymodal/payment.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"payModal\" class=\"modal fade\">\n  <div class=\"modal-dialog\">\n\n    <!-- CREDIT CARD FORM STARTS HERE -->\n    <div class=\"panel panel-default credit-card-box\">\n    <div class=\"panel-heading display-table\" >\n      <div class=\"row display-tr\" >\n        <h3 class=\"panel-title display-td\" >Payment Details</h3>\n        <div class=\"display-td\" >                            \n          <img class=\"img-responsive pull-right\" src=\"http://i76.imgup.net/accepted_c22e0.png\">\n        </div>\n      </div>                    \n    </div>\n    <div class=\"panel-body\">\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"cardNumber\">CARD NUMBER</label>\n            <div class=\"input-group\">\n              <input type=\"tel\" class=\"form-control\" name=\"cardNumber\" placeholder=\"Valid Card Number\" autocomplete=\"cc-number\" required autofocus />\n              <span class=\"input-group-addon\"><i class=\"fa fa-credit-card\"></i></span>\n            </div>\n          </div>                            \n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-7 col-md-7\">\n          <div class=\"form-group\">\n            <label for=\"cardExpiry\"><span class=\"hidden-xs\">EXPIRATION</span><span class=\"visible-xs-inline\">EXP</span> DATE</label>\n            <input type=\"tel\" class=\"form-control\" name=\"cardExpiry\" placeholder=\"MM / YY\" autocomplete=\"cc-exp\" required />\n          </div>\n        </div>\n        <div class=\"col-xs-5 col-md-5 pull-right\">\n          <div class=\"form-group\">\n            <label for=\"cardCVC\">CV CODE</label>\n            <input type=\"tel\" class=\"form-control\" name=\"cardCVC\" placeholder=\"CVC\" autocomplete=\"cc-csc\" required />\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"couponCode\">COUPON CODE</label>\n            <input type=\"text\" class=\"form-control\" name=\"couponCode\" />\n          </div>\n        </div>                        \n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <button class=\"btn btn-success btn-lg btn-block\" (click)=\"redirect()\" data-dismiss=\"modal\">Send Payment</button>\n        </div>\n      </div>\n      <div class=\"row\" style=\"display:none;\">\n        <div class=\"col-xs-12\">\n          <p class=\"payment-errors\"></p>\n        </div>\n      </div>\n    </div>\n    </div>            \n    <!-- CREDIT CARD FORM ENDS HERE -->\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/modals/paymodal/payment.component.ts":
/*!******************************************************!*\
  !*** ./src/app/modals/paymodal/payment.component.ts ***!
  \******************************************************/
/*! exports provided: PaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentComponent", function() { return PaymentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/enviroment-variables */ "./src/app/enviroment-variables.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






let PaymentComponent = class PaymentComponent {
    constructor(cookieService, router, http) {
        this.cookieService = cookieService;
        this.router = router;
        this.http = http;
    }
    ngOnInit() {
        this.modal.subscribe(det => {
            this.fulldetails = this.validateInfo();
            $("#payModal").modal('show');
        }, error => {
            console.log(error);
        });
        this.getUserId();
    }
    redirect() {
        var info = this.validateInfo();
        var total = this.total(info);
        var json = { UserId: this.userId, "Products": info, "Total": total };
        this.http.post(src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_5__["ServerUrl"] + '/Orders/create', json).subscribe(event => {
            if (event === '/thankyou')
                localStorage.clear();
            this.router.navigate([event]);
            location.reload();
        });
    }
    getUserId() {
        this.http.get(src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_5__["ServerUrl"] + '/Customers/UserId').subscribe(data => {
            this.userId = data;
        });
    }
    validateInfo() {
        return (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
    }
    total(products) {
        var total = 0;
        products.forEach(x => total += (x.Price * x.Stock));
        return total;
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"])
], PaymentComponent.prototype, "modal", void 0);
PaymentComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-payment',
        template: __webpack_require__(/*! ./payment.component.html */ "./src/app/modals/paymodal/payment.component.html"),
        styles: [__webpack_require__(/*! ./payment.component.css */ "./src/app/modals/paymodal/payment.component.css")]
    }),
    __metadata("design:paramtypes", [ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], PaymentComponent);



/***/ }),

/***/ "./src/app/modals/productdetails/productdetails.component.css":
/*!********************************************************************!*\
  !*** ./src/app/modals/productdetails/productdetails.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div .modal-image {\n    text-align: center;\n    height: 350px; width: 350px;\n    margin:0 auto;\n}\n\nimg {\n    border: 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kYWxzL3Byb2R1Y3RkZXRhaWxzL3Byb2R1Y3RkZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxtQkFBbUI7SUFDbkIsY0FBYyxDQUFDLGFBQWE7SUFDNUIsY0FBYztDQUNqQjs7QUFFRDtJQUNJLFVBQVU7Q0FDYiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9wcm9kdWN0ZGV0YWlscy9wcm9kdWN0ZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2IC5tb2RhbC1pbWFnZSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGhlaWdodDogMzUwcHg7IHdpZHRoOiAzNTBweDtcbiAgICBtYXJnaW46MCBhdXRvO1xufVxuXG5pbWcge1xuICAgIGJvcmRlcjogMDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/modals/productdetails/productdetails.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/modals/productdetails/productdetails.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"productinfoModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n  \n      <!-- Modal content-->\n      <div class=\"modal-content\" *ngIf=\"fulldetails\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n          <h4 class=\"modal-title\">{{fulldetails.Name}}</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div  class=\"modal-image\"><img src=\"data:image/png;base64,{{fulldetails.ImageUrl}}\" class=\"img-thumbnail\"/></div>\n          <p>{{fulldetails.Details}}</p>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n        </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/modals/productdetails/productdetails.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/modals/productdetails/productdetails.component.ts ***!
  \*******************************************************************/
/*! exports provided: ProductdetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductdetailsComponent", function() { return ProductdetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let ProductdetailsComponent = class ProductdetailsComponent {
    constructor() { }
    ngOnInit() {
        this.list.subscribe(det => {
            //console.log(det);
            this.fulldetails = det;
            $("#productinfoModal").modal('show');
        }, error => {
            console.log(error);
        });
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"])
], ProductdetailsComponent.prototype, "list", void 0);
ProductdetailsComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-productdetails',
        template: __webpack_require__(/*! ./productdetails.component.html */ "./src/app/modals/productdetails/productdetails.component.html"),
        styles: [__webpack_require__(/*! ./productdetails.component.css */ "./src/app/modals/productdetails/productdetails.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ProductdetailsComponent);



/***/ }),

/***/ "./src/app/modals/remove/remove.component.css":
/*!****************************************************!*\
  !*** ./src/app/modals/remove/remove.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9yZW1vdmUvcmVtb3ZlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modals/remove/remove.component.html":
/*!*****************************************************!*\
  !*** ./src/app/modals/remove/remove.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Delete Modal HTML -->\n<div id=\"removeModal\" class=\"modal fade\">\n\t\t<div class=\"modal-dialog\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div *ngIf=\"removedetails !== undefined\">\n\t\t\t\t\t\t\t<form  action=\"{{serverUrl}}/Products/remove/{{removedetails.Id}}\" method=\"GET\">\n\t\t\t\t\t\t\t\t<div class=\"modal-header\">\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<h4 class=\"modal-title\">Delete Product</h4>\n\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"modal-body\">\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<p>Are you sure you want to delete these Records?</p>\n\t\t\t\t\t\t\t\t\t<p class=\"text-warning\"><small>This action cannot be undone.</small></p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t\t\t\t<input type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" value=\"Cancel\">\n\t\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-danger\"  value=\"Delete\" (click)=\"onSubmit(removedetails.Id)\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</form>\n\t\t\t\t\t</div>\n\t\t\t\t\n\t\t\t</div>\n\t\t</div>\n    </div>"

/***/ }),

/***/ "./src/app/modals/remove/remove.component.ts":
/*!***************************************************!*\
  !*** ./src/app/modals/remove/remove.component.ts ***!
  \***************************************************/
/*! exports provided: RemoveComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemoveComponent", function() { return RemoveComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




let RemoveComponent = class RemoveComponent {
    constructor(http) {
        this.http = http;
        this.serverUrl = src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_2__["ServerUrl"];
    }
    ngOnInit() {
        this.removeList.subscribe(det => {
            this.removedetails = det;
            $("#removeModal").modal('show');
        }, error => {
            console.log(error);
        });
    }
    onSubmit(event) {
        console.log(event);
        this.http.get(src_app_enviroment_variables__WEBPACK_IMPORTED_MODULE_2__["ServerUrl"] + '/Products/remove/' + event).subscribe();
        console.log('Ok');
        location.reload();
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"])
], RemoveComponent.prototype, "removeList", void 0);
RemoveComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-remove',
        template: __webpack_require__(/*! ./remove.component.html */ "./src/app/modals/remove/remove.component.html"),
        styles: [__webpack_require__(/*! ./remove.component.css */ "./src/app/modals/remove/remove.component.css")]
    }),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], RemoveComponent);



/***/ }),

/***/ "./src/app/msg/msg.component.css":
/*!***************************************!*\
  !*** ./src/app/msg/msg.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\r\n    box-sizing: border-box;\r\n  }\r\n  \r\n  body {\r\n    padding: 0;\r\n    margin: 0;\r\n  }\r\n  \r\n  #notfound {\r\n    position: relative;\r\n    height: 100vh;\r\n  }\r\n  \r\n  #notfound .notfound {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    -webkit-transform: translate(-50%, -50%);\r\n            transform: translate(-50%, -50%);\r\n  }\r\n  \r\n  .notfound {\r\n    max-width: 520px;\r\n    width: 100%;\r\n    line-height: 1.4;\r\n    text-align: center;\r\n  }\r\n  \r\n  .notfound .notfound-404 {\r\n    position: relative;\r\n    height: 200px;\r\n    margin: 0px auto 20px;\r\n    z-index: -1;\r\n  }\r\n  \r\n  .notfound .notfound-404 h1 {\r\n    font-family: 'Montserrat', sans-serif;\r\n    font-size: 236px;\r\n    font-weight: 200;\r\n    margin: 0px;\r\n    color: #211b19;\r\n    text-transform: uppercase;\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    -webkit-transform: translate(-50%, -50%);\r\n            transform: translate(-50%, -50%);\r\n  }\r\n  \r\n  .notfound .notfound-404 h2 {\r\n    font-family: 'Montserrat', sans-serif;\r\n    font-size: 28px;\r\n    font-weight: 400;\r\n    text-transform: uppercase;\r\n    color: #211b19;\r\n    background: #fff;\r\n    padding: 10px 5px;\r\n    margin: auto;\r\n    display: inline-block;\r\n    position: absolute;\r\n    bottom: 0px;\r\n    left: 0;\r\n    right: 0;\r\n  }\r\n  \r\n  .notfound a {\r\n    font-family: 'Montserrat', sans-serif;\r\n    display: inline-block;\r\n    font-weight: 700;\r\n    text-decoration: none;\r\n    color: #fff;\r\n    text-transform: uppercase;\r\n    padding: 13px 23px;\r\n    background: #ff6300;\r\n    font-size: 18px;\r\n    transition: 0.2s all;\r\n  }\r\n  \r\n  .notfound a:hover {\r\n    color: #ff6300;\r\n    background: #211b19;\r\n  }\r\n  \r\n  @media only screen and (max-width: 767px) {\r\n    .notfound .notfound-404 h1 {\r\n      font-size: 148px;\r\n    }\r\n  }\r\n  \r\n  @media only screen and (max-width: 480px) {\r\n    .notfound .notfound-404 {\r\n      height: 148px;\r\n      margin: 0px auto 10px;\r\n    }\r\n    .notfound .notfound-404 h1 {\r\n      font-size: 86px;\r\n    }\r\n    .notfound .notfound-404 h2 {\r\n      font-size: 16px;\r\n    }\r\n    .notfound a {\r\n      padding: 7px 15px;\r\n      font-size: 14px;\r\n    }\r\n  }\r\n  \r\n  .notfound{\r\n    margin: auto;\r\n    width: 50%;\r\n    padding: 10px;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXNnL21zZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBRVksdUJBQXVCO0dBQ2hDOztFQUVEO0lBQ0UsV0FBVztJQUNYLFVBQVU7R0FDWDs7RUFFRDtJQUNFLG1CQUFtQjtJQUNuQixjQUFjO0dBQ2Y7O0VBRUQ7SUFDRSxtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFNBQVM7SUFDVCx5Q0FBeUM7WUFFakMsaUNBQWlDO0dBQzFDOztFQUVEO0lBQ0UsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsbUJBQW1CO0dBQ3BCOztFQUVEO0lBQ0UsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxzQkFBc0I7SUFDdEIsWUFBWTtHQUNiOztFQUVEO0lBQ0Usc0NBQXNDO0lBQ3RDLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsWUFBWTtJQUNaLGVBQWU7SUFDZiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixTQUFTO0lBQ1QseUNBQXlDO1lBRWpDLGlDQUFpQztHQUMxQzs7RUFFRDtJQUNFLHNDQUFzQztJQUN0QyxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osUUFBUTtJQUNSLFNBQVM7R0FDVjs7RUFFRDtJQUNFLHNDQUFzQztJQUN0QyxzQkFBc0I7SUFDdEIsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0QixZQUFZO0lBQ1osMEJBQTBCO0lBQzFCLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBRWhCLHFCQUFxQjtHQUN0Qjs7RUFFRDtJQUNFLGVBQWU7SUFDZixvQkFBb0I7R0FDckI7O0VBRUQ7SUFDRTtNQUNFLGlCQUFpQjtLQUNsQjtHQUNGOztFQUVEO0lBQ0U7TUFDRSxjQUFjO01BQ2Qsc0JBQXNCO0tBQ3ZCO0lBQ0Q7TUFDRSxnQkFBZ0I7S0FDakI7SUFDRDtNQUNFLGdCQUFnQjtLQUNqQjtJQUNEO01BQ0Usa0JBQWtCO01BQ2xCLGdCQUFnQjtLQUNqQjtHQUNGOztFQUVEO0lBQ0UsYUFBYTtJQUNiLFdBQVc7SUFDWCxjQUFjO0dBQ2YiLCJmaWxlIjoic3JjL2FwcC9tc2cvbXNnLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqIHtcclxuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICB9XHJcbiAgXHJcbiAgYm9keSB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICBcclxuICAjbm90Zm91bmQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICB9XHJcbiAgXHJcbiAgI25vdGZvdW5kIC5ub3Rmb3VuZCB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICB9XHJcbiAgXHJcbiAgLm5vdGZvdW5kIHtcclxuICAgIG1heC13aWR0aDogNTIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5ub3Rmb3VuZCAubm90Zm91bmQtNDA0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICBtYXJnaW46IDBweCBhdXRvIDIwcHg7XHJcbiAgICB6LWluZGV4OiAtMTtcclxuICB9XHJcbiAgXHJcbiAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDEge1xyXG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMjM2cHg7XHJcbiAgICBmb250LXdlaWdodDogMjAwO1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBjb2xvcjogIzIxMWIxOTtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICB9XHJcbiAgXHJcbiAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDIge1xyXG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgY29sb3I6ICMyMTFiMTk7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgcGFkZGluZzogMTBweCA1cHg7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDBweDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICB9XHJcbiAgXHJcbiAgLm5vdGZvdW5kIGEge1xyXG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZjtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBwYWRkaW5nOiAxM3B4IDIzcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmY2MzAwO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjJzIGFsbDtcclxuICAgIHRyYW5zaXRpb246IDAuMnMgYWxsO1xyXG4gIH1cclxuICBcclxuICAubm90Zm91bmQgYTpob3ZlciB7XHJcbiAgICBjb2xvcjogI2ZmNjMwMDtcclxuICAgIGJhY2tncm91bmQ6ICMyMTFiMTk7XHJcbiAgfVxyXG4gIFxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAgIC5ub3Rmb3VuZCAubm90Zm91bmQtNDA0IGgxIHtcclxuICAgICAgZm9udC1zaXplOiAxNDhweDtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0ODBweCkge1xyXG4gICAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQge1xyXG4gICAgICBoZWlnaHQ6IDE0OHB4O1xyXG4gICAgICBtYXJnaW46IDBweCBhdXRvIDEwcHg7XHJcbiAgICB9XHJcbiAgICAubm90Zm91bmQgLm5vdGZvdW5kLTQwNCBoMSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogODZweDtcclxuICAgIH1cclxuICAgIC5ub3Rmb3VuZCAubm90Zm91bmQtNDA0IGgyIHtcclxuICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgfVxyXG4gICAgLm5vdGZvdW5kIGEge1xyXG4gICAgICBwYWRkaW5nOiA3cHggMTVweDtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm5vdGZvdW5ke1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgfVxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/msg/msg.component.html":
/*!****************************************!*\
  !*** ./src/app/msg/msg.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"back\">\n   <a href=\"/cart\">< Back</a>\n</div>\n<hr/>\n         <div class=\"notfound\">\n            <div class=\"notfound-404\">\n               <h2>There is no Stock to satisfy this order</h2>\n            </div>\n            <a href=\"/\">Go TO Homepage</a>\n         </div>\n   "

/***/ }),

/***/ "./src/app/msg/msg.component.ts":
/*!**************************************!*\
  !*** ./src/app/msg/msg.component.ts ***!
  \**************************************/
/*! exports provided: MsgComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MsgComponent", function() { return MsgComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

let MsgComponent = class MsgComponent {
};
MsgComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-msg',
        template: __webpack_require__(/*! ./msg.component.html */ "./src/app/msg/msg.component.html"),
        styles: [__webpack_require__(/*! ./msg.component.css */ "./src/app/msg/msg.component.css")]
    })
], MsgComponent);



/***/ }),

/***/ "./src/app/nav/nav.component.css":
/*!***************************************!*\
  !*** ./src/app/nav/nav.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1{\r\n    color: azure;\r\n    padding-top: 15px;\r\n    padding-left: 100px;\r\n}\r\n\r\nnav{\r\n  height: 200px;\r\n}\r\n\r\na{\r\n  cursor: pointer;\r\n}\r\n\r\n.login {\r\n    background-color: #222;\r\n    position:absolute;\r\n    top:10px;\r\n    right:5%;\r\n}\r\n\r\n.cart-button{\r\n    float: right;\r\n    margin-right: 1%;\r\n    height: 50px;\r\n    width: 12%;\r\n}\r\n\r\n.admin-button {\r\n    position:absolute;\r\n    top: 45px;\r\n    right:24%;\r\n}\r\n\r\n.logo {\r\n    float: left;\r\n    margin-left: 2%;\r\n}\r\n\r\n.links {\r\n  margin-left: 2%;\r\n  position: absolute;\r\n  top: 30px;\r\n  left: 15%;\r\n}\r\n\r\n.logout-label {\r\n    position:absolute;\r\n    top: 65px;\r\n    right: 5.25%;\r\n    color: aliceblue;\r\n}\r\n\r\n.login-label {\r\n    position:absolute;\r\n    top: 65px;\r\n    right: 5.4%;\r\n    color: aliceblue;\r\n}\r\n\r\n.name-label {\r\n    position:absolute;\r\n    top: 10px;\r\n    right:24%;\r\n    color: aliceblue;\r\n}\r\n\r\n.container-1{\r\n  left: 120px;\r\n}\r\n\r\n.itemsPerPage{\r\n  height: 50px;\r\n  width: 85px;\r\n  background: #eff7ff;\r\n  border: none;\r\n  left: 120px;\r\n  border-right: 2px solid gray;\r\n  font-size: 20px;\r\n  /* float: left; */\r\n  color: #63717f;\r\n  /* padding-left: 10px; */\r\n  border-radius: 5px;\r\n  margin-left: 2%;\r\n  z-index: 9999999999;\r\n\r\n\r\n  /* height: 52px;\r\n  width: 80px;\r\n  left: 60px;\r\n  background: #eff7ff;\r\nborder: none;\r\nborder-right: 2px solid gray;\r\nfont-size: 20px;\r\ncolor: #63717f;\r\n-webkit-border-radius: 5px;\r\n-moz-border-radius: 5px;\r\nborder-radius: 5px;\r\nmargin-left: 2%; */\r\n}\r\n\r\n.container-1 select#category{\r\nheight: 50px;\r\nbackground: #eff7ff;\r\nborder: none;\r\nborder-right: 2px solid gray;\r\nfont-size: 20px;\r\ncolor: #63717f;\r\nborder-radius: 5px 0px 0px 5px;\r\nmargin-left: 2%;\r\n}\r\n\r\n.container-1 input#search{\r\nwidth: 100%;\r\nheight: 52px;\r\nbackground: #eff7ff;\r\nborder: none;\r\nfont-size: 20px;\r\nfloat: left;\r\ncolor: #63717f;\r\npadding-left: 20px;\r\nborder-radius: 0px 5px 5px 0px;\r\n/* margin-left: 2%; */\r\n}\r\n\r\n.container-1 input#search:hover, .container-1 input#search:focus, .container-1 input#search:active{\r\n    outline:none;\r\n    background: #ffffff;\r\n  }\r\n\r\n.container-1:hover button.icon, .container-1:active button.icon, .container-1:focus button.icon{\r\n    outline: none;\r\n    opacity: 1;\r\n    margin-left: -50px;\r\n  }\r\n\r\n.container-1:hover button.icon:hover{\r\n    /* background: gray; */\r\n    background: #211b19;\r\n  }\r\n\r\n.glyphicon-search{\r\n    color: #eff7ff;\r\n    }\r\n\r\n.container-1:hover .glyphicon-search:hover{\r\n    color: #ff6300\r\n    }\r\n\r\n.container-1 button.icon{\r\n    -webkit-border-top-right-radius: 5px;\r\n    -webkit-border-bottom-right-radius: 5px;\r\n    -moz-border-radius-topright: 5px;\r\n    -moz-border-radius-bottomright: 5px;\r\n    border-top-right-radius: 5px;\r\n    border-bottom-right-radius: 5px;\r\n   \r\n    border: none;\r\n    /* background: #232833; */\r\n    color: #fff;\r\n    background-color: #5bc0de;\r\n    height: 52px;\r\n    width: 51px;\r\n    color: #4f5b66;\r\n    opacity: 0;\r\n    font-size: 10pt;\r\n    transition: all .55s ease;\r\n  }\r\n\r\nselect {\r\n    background: transparent;\r\n    border: none;\r\n    padding: 6px 12px;\r\n  }\r\n\r\nbutton {\r\n    padding: 6px 12px;\r\n    background: transparent;\r\n    border: none;\r\n  }\r\n\r\n.input-group-addon {\r\n    padding: 0!important;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixvQkFBb0I7Q0FDdkI7O0FBRUQ7RUFDRSxjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxnQkFBZ0I7Q0FDakI7O0FBRUQ7SUFDSSx1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0NBQ1o7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixXQUFXO0NBQ2Q7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFVBQVU7Q0FDYjs7QUFFRDtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7Q0FDbkI7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixVQUFVO0NBQ1g7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGFBQWE7SUFDYixpQkFBaUI7Q0FDcEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFlBQVk7SUFDWixpQkFBaUI7Q0FDcEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFVBQVU7SUFDVixpQkFBaUI7Q0FDcEI7O0FBRUQ7RUFDRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixhQUFhO0VBQ2IsWUFBWTtFQUNaLDZCQUE2QjtFQUM3QixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZix5QkFBeUI7RUFHekIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixvQkFBb0I7OztFQUdwQjs7Ozs7Ozs7Ozs7bUJBV2lCO0NBQ2xCOztBQUVEO0FBQ0EsYUFBYTtBQUNiLG9CQUFvQjtBQUNwQixhQUFhO0FBQ2IsNkJBQTZCO0FBQzdCLGdCQUFnQjtBQUNoQixlQUFlO0FBR2YsK0JBQStCO0FBQy9CLGdCQUFnQjtDQUNmOztBQUVEO0FBQ0EsWUFBWTtBQUNaLGFBQWE7QUFDYixvQkFBb0I7QUFDcEIsYUFBYTtBQUNiLGdCQUFnQjtBQUNoQixZQUFZO0FBQ1osZUFBZTtBQUNmLG1CQUFtQjtBQUduQiwrQkFBK0I7QUFDL0Isc0JBQXNCO0NBQ3JCOztBQUtDO0lBQ0UsYUFBYTtJQUNiLG9CQUFvQjtHQUNyQjs7QUFFRDtJQUNFLGNBQWM7SUFDZCxXQUFXO0lBQ1gsbUJBQW1CO0dBQ3BCOztBQUVEO0lBQ0UsdUJBQXVCO0lBQ3ZCLG9CQUFvQjtHQUNyQjs7QUFFRDtJQUNFLGVBQWU7S0FDZDs7QUFFRDtJQUNBLGNBQWM7S0FDYjs7QUFFSDtJQUNFLHFDQUFxQztJQUNyQyx3Q0FBd0M7SUFDeEMsaUNBQWlDO0lBQ2pDLG9DQUFvQztJQUNwQyw2QkFBNkI7SUFDN0IsZ0NBQWdDOztJQUVoQyxhQUFhO0lBQ2IsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWiwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLFlBQVk7SUFDWixlQUFlO0lBQ2YsV0FBVztJQUNYLGdCQUFnQjtJQU1oQiwwQkFBMEI7R0FDM0I7O0FBSUQ7SUFDRSx3QkFBd0I7SUFDeEIsYUFBYTtJQUNiLGtCQUFrQjtHQUNuQjs7QUFDRDtJQUNFLGtCQUFrQjtJQUNsQix3QkFBd0I7SUFDeEIsYUFBYTtHQUNkOztBQUNEO0lBQ0UscUJBQXFCO0dBQ3RCIiwiZmlsZSI6InNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDF7XHJcbiAgICBjb2xvcjogYXp1cmU7XHJcbiAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgIHBhZGRpbmctbGVmdDogMTAwcHg7XHJcbn1cclxuXHJcbm5hdntcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG59XHJcblxyXG5he1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmxvZ2luIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyMjI7XHJcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgIHRvcDoxMHB4O1xyXG4gICAgcmlnaHQ6NSU7XHJcbn1cclxuXHJcbi5jYXJ0LWJ1dHRvbntcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIG1hcmdpbi1yaWdodDogMSU7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTIlO1xyXG59XHJcblxyXG4uYWRtaW4tYnV0dG9uIHtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgdG9wOiA0NXB4O1xyXG4gICAgcmlnaHQ6MjQlO1xyXG59XHJcblxyXG4ubG9nbyB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyJTtcclxufVxyXG5cclxuLmxpbmtzIHtcclxuICBtYXJnaW4tbGVmdDogMiU7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMzBweDtcclxuICBsZWZ0OiAxNSU7XHJcbn1cclxuXHJcbi5sb2dvdXQtbGFiZWwge1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICB0b3A6IDY1cHg7XHJcbiAgICByaWdodDogNS4yNSU7XHJcbiAgICBjb2xvcjogYWxpY2VibHVlO1xyXG59XHJcblxyXG4ubG9naW4tbGFiZWwge1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICB0b3A6IDY1cHg7XHJcbiAgICByaWdodDogNS40JTtcclxuICAgIGNvbG9yOiBhbGljZWJsdWU7XHJcbn1cclxuXHJcbi5uYW1lLWxhYmVsIHtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgdG9wOiAxMHB4O1xyXG4gICAgcmlnaHQ6MjQlO1xyXG4gICAgY29sb3I6IGFsaWNlYmx1ZTtcclxufVxyXG5cclxuLmNvbnRhaW5lci0xe1xyXG4gIGxlZnQ6IDEyMHB4O1xyXG59XHJcblxyXG4uaXRlbXNQZXJQYWdle1xyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogODVweDtcclxuICBiYWNrZ3JvdW5kOiAjZWZmN2ZmO1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBsZWZ0OiAxMjBweDtcclxuICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCBncmF5O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICAvKiBmbG9hdDogbGVmdDsgKi9cclxuICBjb2xvcjogIzYzNzE3ZjtcclxuICAvKiBwYWRkaW5nLWxlZnQ6IDEwcHg7ICovXHJcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgLW1vei1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAyJTtcclxuICB6LWluZGV4OiA5OTk5OTk5OTk5O1xyXG5cclxuXHJcbiAgLyogaGVpZ2h0OiA1MnB4O1xyXG4gIHdpZHRoOiA4MHB4O1xyXG4gIGxlZnQ6IDYwcHg7XHJcbiAgYmFja2dyb3VuZDogI2VmZjdmZjtcclxuYm9yZGVyOiBub25lO1xyXG5ib3JkZXItcmlnaHQ6IDJweCBzb2xpZCBncmF5O1xyXG5mb250LXNpemU6IDIwcHg7XHJcbmNvbG9yOiAjNjM3MTdmO1xyXG4td2Via2l0LWJvcmRlci1yYWRpdXM6IDVweDtcclxuLW1vei1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbmJvcmRlci1yYWRpdXM6IDVweDtcclxubWFyZ2luLWxlZnQ6IDIlOyAqL1xyXG59XHJcblxyXG4uY29udGFpbmVyLTEgc2VsZWN0I2NhdGVnb3J5e1xyXG5oZWlnaHQ6IDUwcHg7XHJcbmJhY2tncm91bmQ6ICNlZmY3ZmY7XHJcbmJvcmRlcjogbm9uZTtcclxuYm9yZGVyLXJpZ2h0OiAycHggc29saWQgZ3JheTtcclxuZm9udC1zaXplOiAyMHB4O1xyXG5jb2xvcjogIzYzNzE3ZjtcclxuLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbi1tb3otYm9yZGVyLXJhZGl1czogNXB4O1xyXG5ib3JkZXItcmFkaXVzOiA1cHggMHB4IDBweCA1cHg7XHJcbm1hcmdpbi1sZWZ0OiAyJTtcclxufVxyXG5cclxuLmNvbnRhaW5lci0xIGlucHV0I3NlYXJjaHtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogNTJweDtcclxuYmFja2dyb3VuZDogI2VmZjdmZjtcclxuYm9yZGVyOiBub25lO1xyXG5mb250LXNpemU6IDIwcHg7XHJcbmZsb2F0OiBsZWZ0O1xyXG5jb2xvcjogIzYzNzE3ZjtcclxucGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4td2Via2l0LWJvcmRlci1yYWRpdXM6IDVweDtcclxuLW1vei1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbmJvcmRlci1yYWRpdXM6IDBweCA1cHggNXB4IDBweDtcclxuLyogbWFyZ2luLWxlZnQ6IDIlOyAqL1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4gIC5jb250YWluZXItMSBpbnB1dCNzZWFyY2g6aG92ZXIsIC5jb250YWluZXItMSBpbnB1dCNzZWFyY2g6Zm9jdXMsIC5jb250YWluZXItMSBpbnB1dCNzZWFyY2g6YWN0aXZle1xyXG4gICAgb3V0bGluZTpub25lO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICB9XHJcblxyXG4gIC5jb250YWluZXItMTpob3ZlciBidXR0b24uaWNvbiwgLmNvbnRhaW5lci0xOmFjdGl2ZSBidXR0b24uaWNvbiwgLmNvbnRhaW5lci0xOmZvY3VzIGJ1dHRvbi5pY29ue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICBtYXJnaW4tbGVmdDogLTUwcHg7XHJcbiAgfVxyXG4gXHJcbiAgLmNvbnRhaW5lci0xOmhvdmVyIGJ1dHRvbi5pY29uOmhvdmVye1xyXG4gICAgLyogYmFja2dyb3VuZDogZ3JheTsgKi9cclxuICAgIGJhY2tncm91bmQ6ICMyMTFiMTk7XHJcbiAgfVxyXG5cclxuICAuZ2x5cGhpY29uLXNlYXJjaHtcclxuICAgIGNvbG9yOiAjZWZmN2ZmO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250YWluZXItMTpob3ZlciAuZ2x5cGhpY29uLXNlYXJjaDpob3ZlcntcclxuICAgIGNvbG9yOiAjZmY2MzAwXHJcbiAgICB9XHJcblxyXG4gIC5jb250YWluZXItMSBidXR0b24uaWNvbntcclxuICAgIC13ZWJraXQtYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDVweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDVweDtcclxuICAgIC1tb3otYm9yZGVyLXJhZGl1cy10b3ByaWdodDogNXB4O1xyXG4gICAgLW1vei1ib3JkZXItcmFkaXVzLWJvdHRvbXJpZ2h0OiA1cHg7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDVweDtcclxuICAgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiAjMjMyODMzOyAqL1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWJjMGRlO1xyXG4gICAgaGVpZ2h0OiA1MnB4O1xyXG4gICAgd2lkdGg6IDUxcHg7XHJcbiAgICBjb2xvcjogIzRmNWI2NjtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICBmb250LXNpemU6IDEwcHQ7XHJcbiAgIFxyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjU1cyBlYXNlO1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiBhbGwgLjU1cyBlYXNlO1xyXG4gICAgLW1zLXRyYW5zaXRpb246IGFsbCAuNTVzIGVhc2U7XHJcbiAgICAtby10cmFuc2l0aW9uOiBhbGwgLjU1cyBlYXNlO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC41NXMgZWFzZTtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgc2VsZWN0IHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgcGFkZGluZzogNnB4IDEycHg7XHJcbiAgfVxyXG4gIGJ1dHRvbiB7XHJcbiAgICBwYWRkaW5nOiA2cHggMTJweDtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICAuaW5wdXQtZ3JvdXAtYWRkb24ge1xyXG4gICAgcGFkZGluZzogMCFpbXBvcnRhbnQ7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/nav/nav.component.html":
/*!****************************************!*\
  !*** ./src/app/nav/nav.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md navbar-inverse fixed-top\">\n    <div> \n        <a href=\"/\"><img class=\"logo\" src=\"https://flaticons.net/gd/makefg.php?i=icons/Miscellaneous/Gift.png&r=255&g=255&b=255\" height=\"50\" width=\"50\" /><h1 class=\"text-left\">{{ title }}</h1></a>\n             \n        <div class=\"links\">\n                <ul class=\"nav navbar-nav\">\n                    <li><a (click)=\" getDetailsNav( 'none' , 'all' )\">Home</a></li>\n                    <li><a (click)=\"getDetailsNav( 'none' , 'electronics' )\">Electronics</a></li>\n                    <li><a (click)=\"getDetailsNav( 'none' , 'home' )\">Home</a></li>\n                    <li><a (click)=\"getDetailsNav( 'none' , 'accesories' )\">Accesories</a></li>\n                </ul> \n            </div>\n    </div>\n\n  \n        \n    <div *ngIf=\"loged; else loggedOut\"> \n            <h4 class=\"name-label\">Welcome {{ loged }}</h4>\n            <a  *ngIf=\"admin\" href=\"{{serverUrl}}/Customers/admin\" class=\"btn btn-info admin-button\" >\n                <span class=\"\">Go to admin page</span> \n            </a>\n            <a class=\"login\" (click)=\"logOff()\"><img src=\"https://www.confidentcontracts.com/cache/image-full-83-8795a4a12aa745a0852058bd5fc47701.png\" height=\"50\" width=\"50\"/></a>\n            <span class=\"logout-label\">Logout</span>\n    </div>\n\n    <ng-template #loggedOut>\n            <a class=\"login\" href= \"{{serverUrl}}/Account/login\"><img src=\"https://www.confidentcontracts.com/cache/image-full-83-8795a4a12aa745a0852058bd5fc47701.png\" height=\"50\" width=\"50\"/></a>\n            <span class=\"login-label\">Login</span>\n    </ng-template>\n\n    <br>\n\n    <div> \n        <a href=\"/cart\" class=\"btn btn-info btn-lg cart-button\" ><span>( {{totalCart}} )  </span>\n            <span class=\"glyphicon glyphicon-shopping-cart\"></span> Shopping Cart\n        </a>\n        \n     </div>\n\n    <div class=\"row\"  *ngIf=\"isCart != true\">\n            <div class=\"col-xs-2 itemsPerPage\">\n                <select  name=\"itemsPerPage\" #itemsPerPageElement on-change=\"changeNumber()\" value=\"{{this.itemsPerPage}}\">\n                    <option value=\"1\">1</option>\n                    <option value=\"2\">2</option>\n                    <option value=\"3\">3</option>\n                    <option value=\"4\">4</option>\n                    <option value=\"5\">5</option>\n                    <option value=\"6\">6</option>\n                    <option value=\"7\">7</option>\n                    <option value=\"8\">8</option>\n                    <option value=\"9\">9</option>\n                    <option value=\"10\">10</option>\n                    <option value=\"11\">11</option>\n                    <option value=\"12\">12</option>\n                    <option value=\"13\">13</option>\n                    <option value=\"14\">14</option>\n                    <option value=\"15\">15</option>\n                </select> \n            </div>\n           \n        <!-- <div class=\"col-xs-2\"></div> -->\n        <div class=\"col-md-8\">\n          <div class=\"input-group container-1\" >\n            <span class=\"input-group-addon\">\n                <select id=\"category\" #categoryinput>\n                <option value=\"all\">All</option>\n                <option value=\"electronics\">Electronics</option>\n                <option value=\"home\">Home</option>\n                <option value=\"accesories\">Accesories</option>\n                </select>\n            </span>\n\n                <input type=\"text\"  id=\"search\" placeholder=\"    Search...\" #searchinput>\n                <span class=\"input-group-btn\">\n                    <button class=\"icon\" type=\"submit\" (click)=\"getDetails( searchinput , categoryinput )\"><span class=\"glyphicon glyphicon-search\"></span></button>\n                </span>\n          </div>\n        </div>\n        <div class=\"col-md-2\"></div>\n    </div>\n\n    \n    \n    \n</nav>\n"

/***/ }),

/***/ "./src/app/nav/nav.component.ts":
/*!**************************************!*\
  !*** ./src/app/nav/nav.component.ts ***!
  \**************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _enviroment_variables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _ioservices__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ioservices */ "./src/app/ioservices.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







let NavComponent = class NavComponent {
    constructor(data, http, ioservices, router, cookieService) {
        this.data = data;
        this.http = http;
        this.ioservices = ioservices;
        this.router = router;
        this.cookieService = cookieService;
        this.title = 'Gift Shop';
        this.allProducts = [];
        this.admin = false;
        this.serverUrl = _enviroment_variables__WEBPACK_IMPORTED_MODULE_5__["ServerUrl"];
        this.itemsPerPage = 5;
        this.isCart = false;
        this.isLoged();
    }
    ngOnInit() {
        this.data.currentMessage.subscribe(message => this.totalCart = +message);
        if (localStorage.getItem('info') != null) {
            var info = JSON.parse(localStorage.getItem('info'));
            this.totalCart = +info.totalCart;
            localStorage.setItem('info', JSON.stringify({ totalCart: this.totalCart, items: info.items }));
        }
        this.ioservices.inCart.subscribe(event => {
            this.isCart = (event === 'inCart') ? true : false;
        });
        this.itemsPerPage = parseInt(this.cookieService.get("itemsPerPage"));
        this.isLoged();
        this.isAdmin();
    }
    changeNumber() {
        this.itemsPerPage = this.elementItemsPerPage.nativeElement.value;
        this.cookieService.set("itemsPerPage", this.itemsPerPage.toString());
        location.reload();
    }
    getDetails(searchKeyword, categoySelect) {
        var search = (searchKeyword.value == '') ? 'none' : searchKeyword.value.toLowerCase();
        this.router.navigate(['/search', { keyword: search, categoy: categoySelect.value }]);
    }
    getDetailsNav(searchKeyword, categoySelect) {
        this.router.navigate(['/search', { keyword: searchKeyword, categoy: categoySelect }]);
    }
    isLoged() {
        this.http.get(_enviroment_variables__WEBPACK_IMPORTED_MODULE_5__["ServerUrl"] + '/Customers/isLoged/').subscribe(data => {
            this.loged = data;
        });
    }
    isAdmin() {
        this.http.get(_enviroment_variables__WEBPACK_IMPORTED_MODULE_5__["ServerUrl"] + '/Customers/isAdmin/').subscribe(data => {
            if (this.loged != '' && data == 1)
                this.admin = true;
        });
    }
    logOff() {
        this.http.get(_enviroment_variables__WEBPACK_IMPORTED_MODULE_5__["ServerUrl"] + '/Account/LogOff').subscribe();
        localStorage.clear();
        location.reload();
        this.router.navigate(['/']);
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('itemsPerPageElement'),
    __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
], NavComponent.prototype, "elementItemsPerPage", void 0);
NavComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-nav',
        template: __webpack_require__(/*! ./nav.component.html */ "./src/app/nav/nav.component.html"),
        styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/nav/nav.component.css")]
    }),
    __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _ioservices__WEBPACK_IMPORTED_MODULE_6__["IOservices"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
], NavComponent);



/***/ }),

/***/ "./src/app/productlist/productlist.component.css":
/*!*******************************************************!*\
  !*** ./src/app/productlist/productlist.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.pay {\n    text-align: right;\n}\n\n.btn.plus{\n    width: 40px;\n}\n\n.btn.minus{\n    width: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdGxpc3QvcHJvZHVjdGxpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtDQUNyQjs7QUFFRDtJQUNJLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLFlBQVk7Q0FDZiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RsaXN0L3Byb2R1Y3RsaXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYucGF5IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmJ0bi5wbHVze1xuICAgIHdpZHRoOiA0MHB4O1xufVxuXG4uYnRuLm1pbnVze1xuICAgIHdpZHRoOiA0MHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/productlist/productlist.component.html":
/*!********************************************************!*\
  !*** ./src/app/productlist/productlist.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"back\">\n   <a href=\"/\">< Back</a>\n</div>\n<hr/>\n<table class=\"table table-striped\">\n        <thead class=\"thead-dark\">\n          <tr>\n            <!-- <th>ID</th> -->\n            <th>Image</th>\n            <th>Name</th>\n            <th>Price</th>\n            <th colspan=\"2\" class=\"text-center\">Quantity</th>\n            <th class=\"text-center\">Seller limit</th>\n            <th>Action</th>\n          </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let list of products\">\n              <!-- <td>{{list.Id}}</td> -->\n              <td><img src=\"data:image/png;base64,{{list.ImageUrl}}\" class=\"img-thumbnail\" style=\"height:100px;\" /></td>\n              <td>{{list.Name}}</td>\n              <td>{{list.Price | currency}}</td>\n\n              <td [ngClass]=\"{'justify-content-end': true, 'text-right': true }\">\n                  <div *ngIf=\"limitAdd( list.RealQty , list.Stock )  else notShowPlus\">\n                    <button class=\"btn btn-success btn-xs plus\" (click)=\"add(list.Id)\"><i class=\"glyphicon glyphicon-plus\"></i></button>\n                    <div *ngIf=\"list.RealQty < list.Stock  else notShowStock\">\n                        <b style=\"color:red;\"> {{list.Stock}}</b>\n                      </div>\n                      <ng-template #notShowStock>\n                          <b > {{list.Stock}}</b>\n                        </ng-template>\n                  </div>\n              \n                <ng-template #notShowPlus>\n                  <button class=\"btn btn-success btn-xs plus\" (click)=\"add(list.Id)\" disabled><i class=\"glyphicon glyphicon-plus\"></i></button>\n                  <div *ngIf=\"list.RealQty < list.Stock  else notShowStock\">\n                      <b style=\"color:red;\"> {{list.Stock}}</b>\n                    </div>\n                  <ng-template #notShowStock>\n                      <b > {{list.Stock}}</b>\n                    </ng-template>\n                </ng-template>\n                </td>\n                <td>\n                <div *ngIf=\"limitDel(  list.Stock  )  else notShowMinus\">\n                    <button class=\"btn btn-danger btn-xs minus\" (click)=\"del(list.Id)\"><i class=\"glyphicon glyphicon-minus\"></i></button>\n                  </div>\n  \n                  <ng-template #notShowMinus>\n                    <button class=\"btn btn-danger btn-xs minus\" (click)=\"del(list.Id)\" disabled><i class=\"glyphicon glyphicon-minus\"></i></button>\n                  </ng-template>\n              </td>\n\n              <td [ngClass]=\"{'justify-content-end': true, 'text-center': true }\">\n                \n                  <div *ngIf=\"list.Quantity < 0  else notShowQuantity\">\n                      <b style=\"color:red;\"> {{list.Quantity}}</b>\n                    </div>\n                \n                  <ng-template #notShowQuantity>\n                    <b> {{list.Quantity}}</b>\n                  </ng-template>\n              </td>\n              <td>\n                <button class=\"btn btn-primary btn-sm\" (click)=\"getpopup(list)\">View</button> &nbsp;\n                <button class=\"btn btn-danger btn-sm\" (click)=\"delpopup(list.Id)\">Delete</button>\n              </td>\n            </tr>\n        </tbody>\n        <tfoot>\n          <tr>\n            <td>Total Price</td>\n            <td colspan=\"6\" class=\"text-right\"><b>{{ total | currency }}</b></td>\n          </tr>\n        </tfoot>\n      </table>\n\n     \n\n      <div *ngIf=\"loged; else loggedOut\"> \n          <div  *ngIf=\"messageActive() else oktemplate\">\n              <h3 class=\"card-text\" style=\"color: red;\">This Seller can not satisfy this order <i class=\"fa fa-ban\" aria-hidden=\"true\"></i></h3>\n              <button class=\"btn btn-danger btn-lg btn-block\" >Please check your products</button>\n            </div>\n\n          <ng-template #oktemplate>\n             \n          </ng-template>\n\n          <button class=\"btn btn-success btn-lg btn-block\" (click)=\"getpopupPay(payment)\">Pay</button>\n       </div>\n\n  <ng-template #loggedOut>\n      <!-- <form action=\"{{serverUrl}}/Account/login\" method=\"GET\" (validSubmit)=\"onSubmit(editdetails.Id)\"> -->\n        <!-- <button class=\"btn btn-primary btn-sm form-control\" routerLink=\"{{serverUrl}}/Account/login\" type=\"submit\">Please login</button> -->\n\n        <a class=\"btn btn-primary btn-sm form-control\" href= \"{{serverUrl}}/Account/login\">Please login</a>\n      <!-- </form> -->\n  </ng-template>\n\n  <app-productdetails [list] = \"selectedProduct\"></app-productdetails>\n  <app-payment [modal] = \"payModal\"></app-payment>\n\n"

/***/ }),

/***/ "./src/app/productlist/productlist.component.ts":
/*!******************************************************!*\
  !*** ./src/app/productlist/productlist.component.ts ***!
  \******************************************************/
/*! exports provided: ProductlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductlistComponent", function() { return ProductlistComponent; });
/* harmony import */ var _ioservices__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../ioservices */ "./src/app/ioservices.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _enviroment_variables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








let ProductlistComponent = class ProductlistComponent {
    constructor(data, cookieService, http, router, ioservices) {
        this.data = data;
        this.cookieService = cookieService;
        this.http = http;
        this.router = router;
        this.ioservices = ioservices;
        this.totalCart = 0;
        this.selectedProduct = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"];
        this.payModal = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"];
        this.total = 0;
        this.loged = false;
        this.serverUrl = _enviroment_variables__WEBPACK_IMPORTED_MODULE_6__["ServerUrl"];
        this.totalCanAdd = 0;
        if (localStorage.getItem('info') != null) {
            var info = JSON.parse(localStorage.getItem('info'));
            this.products = info.items;
            this.totalCart = info.totalCart;
        }
    }
    ngOnInit() {
        this.totalPrice();
        this.isLoged();
        this.getAllProducts();
        this.ioservices.sendMessage();
    }
    getpopup(det) {
        this.selectedProduct.next(det);
    }
    getpopupPay(det) {
        this.payModal.next(det);
        ;
    }
    delpopup(pid) {
        var total = 0;
        var info = JSON.parse(localStorage.getItem('info'));
        this.products.forEach((element, index) => {
            if (element.Id == pid) {
                this.products.splice(index, 1);
                total += element.Stock;
            }
        });
        this.updateCart(info.totalCart - total);
    }
    totalPrice() {
        this.total = 0;
        if (localStorage.getItem('info') != null)
            this.products.forEach(element => this.total += (element.Price * element.Stock));
    }
    add(pid) {
        var total = 0;
        this.products.forEach(element => {
            var pro = this.productById(element.Id);
            element.Stock += (element.Id === pid && element.Stock >= 1) ? 1 : 0;
            total += element.Stock;
            element.Quantity = pro.Stock - element.Stock;
            element.RealQty = pro.Stock;
        });
        this.updateCart(total);
    }
    del(pid) {
        var total = 0;
        this.products.forEach(element => {
            var pro = this.productById(element.Id);
            element.Stock -= (element.Id === pid && element.Stock >= 1) ? 1 : 0;
            total += element.Stock;
            element.Quantity = pro.Stock - element.Stock;
            element.RealQty = pro.Stock;
        });
        this.updateCart(total);
    }
    updateCart(total) {
        this.totalPrice();
        this.data.changeMessage(total);
        //--- Set info on cookie to update Cart
        localStorage.setItem('info', JSON.stringify({ totalCart: total, items: this.products }));
    }
    productById(id) {
        var product;
        this.allProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
    isLoged() {
        this.http.get(this.serverUrl + '/Customers/isLoged/').subscribe(data => {
            this.loged = data;
        });
    }
    getAllProducts() {
        this.http.get(this.serverUrl + '/Products/all').subscribe(data => {
            this.allProducts = data;
        });
    }
    limitDel(num) {
        return (num == 1) ? false : true;
    }
    limitAdd(qty, num) {
        return (qty > num) ? true : false;
    }
    messageActive() {
        var flag = false;
        this.products.forEach(element => {
            if (element.Quantity < 0)
                flag = true;
        });
        return flag;
    }
};
ProductlistComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-productlist',
        template: __webpack_require__(/*! ./productlist.component.html */ "./src/app/productlist/productlist.component.html"),
        styles: [__webpack_require__(/*! ./productlist.component.css */ "./src/app/productlist/productlist.component.css")]
    }),
    __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], _ioservices__WEBPACK_IMPORTED_MODULE_0__["IOservices"]])
], ProductlistComponent);



/***/ }),

/***/ "./src/app/search/search.component.css":
/*!*********************************************!*\
  !*** ./src/app/search/search.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".div-images {\n    height: 350px; width: 350px;\n    margin:0 auto;\n    \n}\n\nimg {\n    border: 0;\n}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VhcmNoL3NlYXJjaC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYyxDQUFDLGFBQWE7SUFDNUIsY0FBYzs7Q0FFakI7O0FBRUQ7SUFDSSxVQUFVO0NBQ2IiLCJmaWxlIjoic3JjL2FwcC9zZWFyY2gvc2VhcmNoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGl2LWltYWdlcyB7XG4gICAgaGVpZ2h0OiAzNTBweDsgd2lkdGg6IDM1MHB4O1xuICAgIG1hcmdpbjowIGF1dG87XG4gICAgXG59XG5cbmltZyB7XG4gICAgYm9yZGVyOiAwO1xufVxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/search/search.component.html":
/*!**********************************************!*\
  !*** ./src/app/search/search.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"back\">\n\t<a href=\"/\">< Back</a>\n</div>\n<hr/>\n      <div class=\"row\">\n          <div class=\"col-sm-6\" *ngFor=\"let list of allProducts | paginate: { itemsPerPage: itemsPerPage, currentPage: p }\">\n              <div class=\"card\" style=\"width: 50rem;\">\n                <div class=\"div-images\">\n                  <img  src=\"data:image/png;base64,{{list.ImageUrl}}\" class=\"img-thumbnail\" />\n                </div>\n                  <div class=\"card-body\">\n                    <h5 class=\"card-title\"><h4>{{list.Name}} - {{list.Id}} ( {{list.Category}} )</h4></h5>\n                    <p class=\"card-text\">{{list.Price | currency}}</p>\n                   \n                    <div *ngIf=\"stock(list.Id)  else notShow\">\n                      <p class=\"card-text\" style=\"color: green;\">Available <i class=\"fa fa-check-circle-o\" aria-hidden=\"true\"></i></p>\n                    </div>\n\n                    <ng-template #notShow>\n                      <p class=\"card-text\" style=\"color: red;\">Unavailable <i class=\"fa fa-times-circle-o\" aria-hidden=\"true\"></i></p>\n                    </ng-template>\n                    \n                    <button class=\"btn btn-primary btn-sm form-control\"  routerLink=\"/{{list.Id}}\">View info</button>\n                    <br><br>\n\n                    <div *ngIf=\"stock(list.Id)  else notShowInput\">\n                      <button class=\"btn btn-success btn-sm form-control\" (click)=\"newMessage($event)\" value=\"{{list.Id}}\">Add to Cart</button>\n                    </div>\n\n                    <ng-template #notShowInput>\n                      <button class=\"btn btn-success btn-sm form-control\" value=\"{{list.Id}}\" disabled>Add to Cart</button>\n                    </ng-template>\n\n                    <hr> <br> <br>\n                  </div>\n                </div>\n          </div>\n      </div>\n\n  <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\n  <app-productdetails [list] = \"selectedProduct\"></app-productdetails>\n\n"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/*!********************************************!*\
  !*** ./src/app/search/search.component.ts ***!
  \********************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _enviroment_variables__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../enviroment-variables */ "./src/app/enviroment-variables.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _cookiemanager__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../cookiemanager */ "./src/app/cookiemanager.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








let SearchComponent = class SearchComponent {
    constructor(el, data, cookieService, http, cookieManager, route) {
        this.el = el;
        this.data = data;
        this.cookieService = cookieService;
        this.http = http;
        this.cookieManager = cookieManager;
        this.route = route;
        this.totalCart = 0;
        this.cartProducts = [];
        this.selectedProduct = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"];
        this.allProducts = [];
        this.realProducts = [];
        this.serverUrl = _enviroment_variables__WEBPACK_IMPORTED_MODULE_4__["ServerUrl"];
        this.getAllRealProducts();
        this.getSearch();
    }
    ngOnInit() {
        if (localStorage.getItem('info') != null) {
            var info = JSON.parse(localStorage.getItem('info')); // Json cookie
            this.totalCart = info.totalCart;
        }
        this.getAllRealProducts();
        this.itemsPerPage = parseInt(this.cookieService.get("itemsPerPage"));
    }
    getSearch() {
        this.route.params.subscribe(params => {
            this.keyword = params['keyword'] !== undefined ? params['keyword'] : 'none';
            this.categoy = params['categoy'] !== undefined ? params['categoy'] : '';
            this.http.get(_enviroment_variables__WEBPACK_IMPORTED_MODULE_4__["ServerUrl"] + '/Products/search/' + this.categoy + '/' + this.keyword).subscribe(data => {
                data.forEach(element => element.Stock = (element.Stock > 0) ? 1 : 0);
                this.allProducts = (data.length != 0) ? data : [];
            });
        });
    }
    newMessage(event) {
        this.add(event.target.value);
        // if( localStorage.getItem('info') != null){
        //   var info =  JSON.parse(localStorage.getItem('info')); // Json cookie
        //   this.totalCart = info.totalCart + 1;
        //   this.data.changeMessage( this.totalCart.toString() )
        //   localStorage.setItem('info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
        // }
    }
    getpopup(det) {
        this.selectedProduct.next(det);
    }
    add(pid) {
        let flag = false;
        // ---Validate if Json on cookie is empty, in that case work with this.cartProduct list
        this.cartProducts = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
        this.allProducts.forEach(element => {
            if (element.Id == pid) {
                // ---If cartProducts is empty then put element inside list on else
                if (this.cartProducts.length != 0) {
                    for (let cartProd of this.cartProducts) {
                        // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
                        if (cartProd.Id == element.Id) {
                            var product = this.productById(element.Id);
                            cartProd.Stock += 1;
                            cartProd.Quantity = product.Stock - cartProd.Stock;
                            cartProd.RealQty = product.Stock;
                            flag = false;
                            break;
                        }
                        else {
                            flag = true;
                        }
                    }
                    if (flag)
                        this.addCart(element);
                }
                else {
                    this.addCart(element);
                }
            }
        });
        this.totalCart += 1;
        //---Send msg to change status Cart
        this.data.changeMessage(this.totalCart.toString());
        // --- Set info on cookie to update Cart
        localStorage.setItem('info', JSON.stringify({ totalCart: this.totalCart, items: this.cartProducts }));
        // this.cookieService.set( 'info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
        // console.log(  localStorage.getItem('info') );
    }
    addCart(product) {
        var prod = this.productById(product.Id);
        product.Quantity = prod.Stock - product.Stock;
        product.RealQty = prod.Stock;
        this.cartProducts.push(product);
    }
    getAllRealProducts() {
        this.http.get(this.serverUrl + '/Products/all').subscribe(data => {
            this.realProducts = data;
        });
    }
    stock(id) {
        var cartProduct;
        this.cartProducts = (localStorage.getItem('info') != null) ? JSON.parse(localStorage.getItem('info')).items : [];
        this.allProducts.forEach(element => {
            if (element.Id == id) {
                // ---If cartProducts is empty then put element inside list on else
                if (this.cartProducts.length != 0) {
                    for (let cartProd of this.cartProducts) {
                        // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
                        if (cartProd.Id == element.Id) {
                            var product = this.productById(element.Id);
                            if (product != undefined) {
                                //cartProd.Stock += 1;
                                cartProd.Quantity = product.Stock - cartProd.Stock;
                                cartProd.RealQty = product.Stock;
                                cartProduct = cartProd;
                                break;
                            }
                        }
                    }
                }
            }
        });
        var qty;
        if (cartProduct != undefined) {
            qty = cartProduct.Quantity;
        }
        else {
            var aux = this.cartProductById(id);
            if (aux != undefined)
                qty = aux.Stock;
        }
        return (qty > 0) ? true : false;
    }
    productById(id) {
        var product;
        this.realProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
    cartProductById(id) {
        var product;
        this.realProducts.forEach(element => { if (element.Id == id)
            product = element; });
        return product;
    }
};
SearchComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-search',
        template: __webpack_require__(/*! ./search.component.html */ "./src/app/search/search.component.html"),
        styles: [__webpack_require__(/*! ./search.component.css */ "./src/app/search/search.component.css")]
    }),
    __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _cookiemanager__WEBPACK_IMPORTED_MODULE_6__["CookieManager"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]])
], SearchComponent);



/***/ }),

/***/ "./src/app/thankyou/thankyou.component.css":
/*!*************************************************!*\
  !*** ./src/app/thankyou/thankyou.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.thankyou {\r\n    text-align: center;\r\n}\r\n\r\na.btn-sm {\r\n    font-family: 'Montserrat', sans-serif;\r\n    display: inline-block;\r\n    font-weight: 700;\r\n    text-decoration: none;\r\n    color: #fff;\r\n    text-transform: uppercase;\r\n    padding: 13px 23px;\r\n    background: #ff6300;\r\n    font-size: 18px;\r\n    transition: 0.2s all;\r\n}\r\n\r\na.btn-sm:hover {\r\n    color: #ff6300;\r\n    background: #211b19;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGhhbmt5b3UvdGhhbmt5b3UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLHNDQUFzQztJQUN0QyxzQkFBc0I7SUFDdEIsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0QixZQUFZO0lBQ1osMEJBQTBCO0lBQzFCLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBRWhCLHFCQUFxQjtDQUN4Qjs7QUFFRDtJQUNJLGVBQWU7SUFDZixvQkFBb0I7Q0FDdkIiLCJmaWxlIjoic3JjL2FwcC90aGFua3lvdS90aGFua3lvdS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2LnRoYW5reW91IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuYS5idG4tc20ge1xyXG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZjtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBwYWRkaW5nOiAxM3B4IDIzcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmY2MzAwO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjJzIGFsbDtcclxuICAgIHRyYW5zaXRpb246IDAuMnMgYWxsO1xyXG59XHJcblxyXG5hLmJ0bi1zbTpob3ZlciB7XHJcbiAgICBjb2xvcjogI2ZmNjMwMDtcclxuICAgIGJhY2tncm91bmQ6ICMyMTFiMTk7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/thankyou/thankyou.component.html":
/*!**************************************************!*\
  !*** ./src/app/thankyou/thankyou.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-xs-center thankyou\" >\n  <h1 class=\"display-3\">Thank You!</h1>\n  <p class=\"lead\"><strong>Please check your email</strong> to download your bill.</p>\n  <hr>\n  <p>\n    Having trouble? <a href=\"\">Contact us</a>\n  </p>\n  <p class=\"lead\">\n    <a class=\"btn btn-sm\" href=\"/\" role=\"button\">Continue to homepage</a>\n  </p>\n</div>"

/***/ }),

/***/ "./src/app/thankyou/thankyou.component.ts":
/*!************************************************!*\
  !*** ./src/app/thankyou/thankyou.component.ts ***!
  \************************************************/
/*! exports provided: ThankyouComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThankyouComponent", function() { return ThankyouComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

let ThankyouComponent = class ThankyouComponent {
};
ThankyouComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-thankyou',
        template: __webpack_require__(/*! ./thankyou.component.html */ "./src/app/thankyou/thankyou.component.html"),
        styles: [__webpack_require__(/*! ./thankyou.component.css */ "./src/app/thankyou/thankyou.component.css")]
    })
], ThankyouComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\vhenriquez\Documents\giftShop\SS.Mvc.Template1\SS.Mvc.Template1\App_GiftShop\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map