/// <binding ProjectOpened='default' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require("gulp");
var sass = require("gulp-sass");
var cssmin = require("gulp-cssmin");
var rename = require("gulp-rename");
var uglify = require("gulp-uglify");
var vendor = require("./vendor.json");

var vendorOriginDir = "./node_modules/",
	vendorTargetDir = "./Scripts/vendor/";

gulp.task("scss", function () {
	return gulp.src("./content/scss/*.scss", { base: "./content/scss" })
		.pipe(sass().on("error", sass.logError))
		.pipe(gulp.dest("./content/css"));
});

gulp.task("css:minify", ["scss"], function () {
	gulp.src("./content/css/**/!(*.min).css")
		.pipe(cssmin())
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest("./content/css"));
});

gulp.task("vendor:copy", function () {
	var vendorAssets = vendor.base.slice(); // Copy all base scripts first

	var lazy = vendor.lazy;// Copy all lazy scripts next
	// JavaScript libraries
	var scripts = lazy.scripts;
	for (var s in scripts) {
		if (scripts.hasOwnProperty(s)) {
			for (var k = 0; k < scripts[s].length; k++) {
				vendorAssets = vendorAssets.concat(scripts[s][k].files);
			}
		}
	}

	// Angular modules
	for (var i = 0; i < lazy.modules.length; i++) {
		var m = lazy.modules[i];
		for (var j = 0; j < m.contents.length; j++) {
			vendorAssets = vendorAssets.concat(m.contents[j].files);
		}
	}

	// Other assets
	vendorAssets = vendorAssets.concat(vendor.other);

	var mapped = vendorAssets.map(function (file) {
		return vendorOriginDir + file;
	});
	gulp.src(mapped, { base: vendorOriginDir })
		.pipe(gulp.dest(vendorTargetDir));
});

gulp.task("vendor:minify:css", ["vendor:copy"], function () {
	gulp.src(vendorTargetDir + "**/!(*.min).css")
		.pipe(cssmin())
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest(vendorTargetDir));
});

gulp.task("vendor:minify:js", ["vendor:copy"], function () {
	gulp.src(vendorTargetDir + "**/!(*.min).js")
		.pipe(uglify())
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest(vendorTargetDir));
});

gulp.task("vendor:minify", ["vendor:minify:js", "vendor:minify:css"]);

gulp.task("vendor", ["vendor:copy", "vendor:minify"]);

gulp.task("default", ["vendor", "scss", "css:minify"]);