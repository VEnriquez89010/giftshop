﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using WebApiSegura.Controllers;

namespace SS.Mvc.Template1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //    name: "account",
            //     url: "account/{action}/{id}",
            //    defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "error",
                 url: "error/{action}",
                defaults: new { controller = "Error", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                 "SpaRoutes",
                 "{*catchall}",
                 new { controller = "Home", action = "Index" }
            );
        }
    }
}