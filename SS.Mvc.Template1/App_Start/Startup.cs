﻿using Microsoft.Owin;
using Owin;
using SS.Mvc.core;

[assembly: OwinStartupAttribute(typeof(SS.Mvc.Template1.Startup))]

namespace SS.Mvc.Template1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ContainerConfig.ConfigureDependencyResolver(app);

            ConfigureAuth(app);
        }
    }
}
