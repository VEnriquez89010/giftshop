﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Optimization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SS.Mvc.Template1
{
    public static class BundleConfig
    {
        public static readonly string[] Modules = { "dashboard", "home" };

        public static readonly JObject Lazy = new JObject();

        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            // AngularJS
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/vendor/angular/angular.js",
                "~/Scripts/vendor/angular-cookies/angular-cookies.js",
                "~/Scripts/vendor/angular-local-storage/dist/angular-local-storage.js",
                "~/Scripts/vendor/angular-resource/angular-resource.js",
                "~/Scripts/vendor/angular-sanitize/angular-sanitize.js",
                "~/Scripts/vendor/angular-moment/angular-moment.min.js",
                "~/Scripts/vendor/@uirouter/core/_bundles/ui-router-core.js",
                "~/Scripts/vendor/@uirouter/angularjs/release/ui-router-angularjs.js",
                "~/Scripts/vendor/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js",
                "~/Scripts/vendor/oclazyload/dist/ocLazyLoad.js",
                "~/Scripts/vendor/angular-translate/dist/angular-translate.js",
                "~/Scripts/vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.js",
                "~/Scripts/vendor/angular-translate-storage-local/angular-translate-storage-local.js",
                "~/Scripts/vendor/angular-translate-storage-cookie/angular-translate-storage-cookie.js",
                "~/Scripts/vendor/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.js"));

            //bundles.Add(new ScriptBundle("~/bundles/app").Include(
            //    "~/Scripts/app/layoutFixups.js",
            //    "~/Scripts/app/app.js",
            //    "~/Scripts/app/common/*.js",
            //    "~/Scripts/app/routes/base.js",
            //    "~/Scripts/app/common/services/*.js",
            //    "~/Scripts/app/common/directives/*.js",
            //    "~/Scripts/app/common/filters/*.js"));

            bundles.Add(new StyleBundle("~/Content/css/app.css").Include(
                "~/Content/css/style.css"
                ));

            RegisterLazy(bundles);
        }

        private static void RegisterLazy(BundleCollection bundles)
        {
            var vendor = JObject.Parse(File.ReadAllText(HttpContext.Current.Server.MapPath("~/vendor.json")));
            var lazy = vendor["lazy"];

            var scripts = RegisterVendor(bundles, (JObject)lazy["scripts"]);
            var modules = RegisterVendorModules(bundles, (JArray)lazy["modules"]);
            RegisterAppModules(bundles, modules);

            Lazy["scripts"] = scripts;
            Lazy["modules"] = modules;
        }

        private static JObject RegisterVendor(BundleCollection bundles, JObject scripts)
        {
            var lazyScripts = new JObject();

            foreach (var prop in scripts.Properties())
            {
                switch (prop.Value.Type)
                {
                    case JTokenType.Object:
                        lazyScripts[prop.Name] = prop.Value;
                        break;

                    case JTokenType.Array:
                        var contents = (JArray)prop.Value;
                        var paths = Register(bundles, contents);
                        lazyScripts[prop.Name] = new JBundle(paths);
                        break;

                    default:
#if DEBUG
						throw new InvalidOperationException($"Invalid type {prop.Value.Type} for vendor script {prop.Name}.");
#else
                        continue;
#endif
                }
            }

            return lazyScripts;
        }

        private static JArray RegisterVendorModules(BundleCollection bundles, JArray modules)
        {
            var lazyModules = new JArray();
            foreach (var module in modules)
            {
                var name = (string)module["name"];
                var contents = (JArray)module["contents"];
                var paths = Register(bundles, contents);
                lazyModules.Add(new JObject
                {
                    ["name"] = name,
                    ["files"] = new JBundle(paths)
                });
            }

            return lazyModules;
        }

        private static void RegisterAppModules(BundleCollection bundles, JArray modules)
        {
            // App modules
            foreach (var module in Modules)
            {
                var path = $"~/bundles/app/{module}.js";
                //bundles.Add(new ScriptBundle(path)
                //    .Include($"~/Scripts/app/modules/{module}/{module}.module.js")
                //    .IncludeDirectory($"~/Scripts/app/modules/{module}", "*.js", false));

                modules.Add(new JObject
                {
                    ["serie"] = !BundleTable.EnableOptimizations,
                    ["name"] = $"app.{module}",
                    ["files"] = new JBundle(new[] { path })
                });
            }
        }

        private static ICollection<string> Register(BundleCollection bundles, JArray contents)
        {
            var paths = new List<string>();
            foreach (var entry in contents)
            {
                Bundle bundle;
                switch ((string)entry["type"])
                {
                    case "css":
                        bundle = new StyleBundle("~/content/" + entry["name"]);
                        break;

                    case "js":
                        bundle = new ScriptBundle("~/bundles/" + entry["name"]);
                        break;

                    default:
#if DEBUG
						throw new InvalidOperationException($"Unknonwn bundle type {entry["type"]} for {entry["name"]}.");
#else
                        continue;
#endif
                }

                foreach (var file in (JArray)entry["files"])
                {
                    bundle.Include("~/Scripts/vendor/" + (string)file);
                }

                paths.Add(bundle.Path);
                bundles.Add(bundle);
            }

            return paths;
        }

        /// <summary>
        /// Represents JSON array containing bundled scripts. The resulting elements in the array may differ depending on the
        /// optimization configuration.
        /// </summary>
        /// <remarks>
        /// The rendering of the bundle scripts cannot be precalculated, it has to be delegated here because some information 
        /// from the curren request is needed.
        /// </remarks>
        /// <seealso cref="Newtonsoft.Json.Linq.JValue" />
        private class JBundle : JValue
        {
            private readonly ICollection<string> _paths;

            public JBundle(ICollection<string> paths)
                : base((string)null)
            {
                _paths = paths;
            }

            public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
            {
                writer.WriteStartArray();
                var values = BundleExtensions.DeterminePathsToRender(_paths);
                foreach (var value in values)
                {
                    writer.WriteValue(value);
                }
                writer.WriteEndArray();
            }

            public override Task WriteToAsync(JsonWriter writer, CancellationToken cancellationToken, params JsonConverter[] converters)
            {
                WriteTo(writer, converters);
                return Task.FromResult(0);
            }
        }
    }
}