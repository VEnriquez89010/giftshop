import { EventEmitter, Injectable, Output, OnInit } from "@angular/core";

@Injectable()
export class IOservices implements OnInit{
   
    
    @Output() inCart = new EventEmitter<any>();    

    constructor() {
        
    }

    ngOnInit(): void {
       
    }

    sendMessage(){
        this.inCart.emit("inCart");
    }
}