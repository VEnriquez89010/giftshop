import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { ServerUrl } from 'src/app/enviroment-variables';
import { HttpClient } from '@angular/common/http';
declare var $: any;

@Component({
  selector: 'app-remove',
  templateUrl: './remove.component.html',
  styleUrls: ['./remove.component.css']
})
export class RemoveComponent implements OnInit {

  @Input() removeList : Subject<any>;
  removedetails : any[];
  serverUrl = ServerUrl;

  constructor( private http: HttpClient ) { }

  ngOnInit() {

    this.removeList.subscribe(
      det => {
        this.removedetails = det; 
        $("#removeModal").modal('show'); 
      },
      error => {
        console.log(error);
      } 
    )
  }

  onSubmit(event) {
    console.log(event);
     this.http.get( ServerUrl + '/Products/remove/' + event ).subscribe( );
     console.log('Ok');
     location.reload();
  }

}
