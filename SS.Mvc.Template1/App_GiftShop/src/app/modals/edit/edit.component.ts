import { IProducts } from './../../products';
import { Component, OnInit, Input } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { ServerUrl } from 'src/app/enviroment-variables';
import {FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpEventType, HttpResponse, HttpEvent, HttpRequest, HttpClient } from '@angular/common/http';
declare var $: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  @Input() editList : Subject<any>;
  editdetails : IProducts[];
  serverUrl = ServerUrl;
  formGroup: FormGroup;
  selectedCategory;
  selectedFiles: FileList;
  currentFileUpload: File;
  @Input() nameFile = 'Chose image';
  progress: { percentage: number} = {percentage: 0};
  urlDocumentController = '/product'
  urlDocument: any;
  realProducts: IProducts[];

  constructor( private http: HttpClient  ) {
    this.getAllRealProducts();
   }

  ngOnInit() {
    this.getAllRealProducts();
    this.editList.subscribe(
      det => {
        this.editdetails = det; 
        this.validator(det.Id);
        this.selectedCategory = det.Category;
        $("#editModal").modal('show'); 
      },
      error => {
        console.log(error);
      } 
    )
    
  }

  validator( id ){
    var product: IProducts = this.productById( id );
    this.formGroup = new FormGroup({
      Id: new FormControl( product.Id, [
        //Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Name: new FormControl( product.Name, [
        //Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Price: new FormControl( product.Price , [
        Validators.minLength(1)
        // Validators.minLength(8),
        // Validators.maxLength(20)
      ]),
      Details: new FormControl( product.Details, [
         Validators.minLength(8),
        // Validators.maxLength(20)
      ]),
      Stock: new FormControl( product.Stock, [
        Validators.minLength(1)
      ]),
      Category: new FormControl( product.Category, []),
      ImageUrl: new FormControl( product.ImageUrl, [
        Validators.minLength(1)
        // Validators.minLength(8),
        // Validators.maxLength(20)
      ])
    });
  }

  onSubmit() {
    console.log(this.formGroup);
    var json = this.formGroup.value;
     this.http.post( ServerUrl + '/Products/edit' , json ).subscribe( );
     console.log('Ok');
     location.reload();
  }
 
  onReset() {
    this.formGroup.reset();
  }

  selectedFile(event){
    const file = event.target.files.item(0);
    if( file.type.match("image.*")){
      this.selectedFiles = event.target.files;
      this.nameFile = event.target.files[0].name;
    }else{
      alert('invalid format!');
    }
  }

  upload(){
    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
   
    this.pushFile( this.serverUrl + '/Products/uploadFile' , this.currentFileUpload ).subscribe( data => {
      if( data.type  ===  HttpEventType.UploadProgress){
        this.progress.percentage = Math.round( 100 * data.loaded / data.total );
      } else if ( data instanceof HttpResponse) {
         console.log('succesful');
         this.urlDocument = data.body;
      }
    });
    this.selectedFiles = undefined;
  }

  pushFile( url: string , file: File ): Observable<HttpEvent<{}>>{
    const formdata: FormData = new FormData();
    formdata.append( file.name , file );
    const req = new HttpRequest( 'POST' , url , formdata , {
      reportProgress: true
    });
    return this.http.request(req);
  }

  getAllRealProducts() {
    this.http.get<IProducts[]>( this.serverUrl + '/Products/all').subscribe(  data => {
      this.realProducts = <IProducts[]>data;
    });
  }

  productById( id ){
    var product: IProducts;
    this.realProducts.forEach( element => { if ( element.Id == id ) product = element; } );
    return product;
  }
}
