import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import {Router} from "@angular/router"
import { IProducts } from 'src/app/products';
import { ServerUrl } from 'src/app/enviroment-variables';

declare var $: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  @Input() modal : Subject<any>;
  fulldetails : any[];
  userId: any;
  

  constructor( private cookieService: CookieService , private router: Router ,private http: HttpClient) { }

  ngOnInit() {

    this.modal.subscribe(
      det => {
        this.fulldetails = this.validateInfo();
        $("#payModal").modal('show'); 
      },
      error => {
        console.log(error);
      } 
    )
    this.getUserId () ;
  }

  redirect(){
    var info = this.validateInfo();
    var total = this.total( info );
    var json = { UserId: this.userId ,"Products" : info , "Total" : total };
    this.http.post( ServerUrl + '/Orders/create' , json ).subscribe( event => {
         if( event === '/thankyou') localStorage.clear();
         this.router.navigate([event]);
         location.reload();
        });
  }

  getUserId () {
     this.http.get( ServerUrl +'/Customers/UserId').subscribe(  data => {
      this.userId = data;
    }); 
  }

  validateInfo(){
    return ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
  }

  total ( products: IProducts[] ) {
    var total = 0;
    products.forEach( x => total += ( x.Price * x.Stock ));
    return total;
  }
}
