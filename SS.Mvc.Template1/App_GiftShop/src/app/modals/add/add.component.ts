import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
declare var $: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  @Input() modal : Subject<any>;
  fulldetails : any[];

  constructor(private http: HttpClient) { }

  ngOnInit() {

    this.modal.subscribe(
      det => {
        this.fulldetails = det; 
        $("#addModal").modal('show'); 
      },
      error => {
        console.log(error);
      } 
    )
  }
}
