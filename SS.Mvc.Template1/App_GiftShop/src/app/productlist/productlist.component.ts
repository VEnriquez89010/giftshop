import { IOservices } from './../ioservices';
import { Component, OnInit, Output } from '@angular/core';
import { IProducts } from '../products';
import { Subject } from 'rxjs';
import { DataService } from '../data.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { ServerUrl } from '../enviroment-variables';
import { Router } from '@angular/router';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {
  totalCart: number = 0;
  products: IProducts[];
  selectedProduct : Subject<any> = new Subject;
  payModal : Subject<any> = new Subject;
  total:number = 0;
  loged: boolean = false;
  serverUrl = ServerUrl ;
  totalCanAdd: number = 0;
  allProducts: IProducts[];


  constructor( public data: DataService , private cookieService: CookieService , private http: HttpClient , private router: Router , private ioservices:  IOservices) { 
      if( localStorage.getItem('info') != null){
        var info =  JSON.parse( localStorage.getItem('info') );
        this.products = info.items;
        this.totalCart = info. totalCart;
    }
   }

  ngOnInit() {
    this.totalPrice();
    this.isLoged();
    this.getAllProducts();
    this.ioservices.sendMessage();
  }

  getpopup( det ) {
    this.selectedProduct.next( det );
  }

  
  getpopupPay( det ) {
    this.payModal.next( det );;
  }

  delpopup( pid ){
    var total = 0;
    var info =  JSON.parse(localStorage.getItem('info'));
    this.products.forEach( ( element , index ) => {
      if( element.Id == pid ) {
        this.products.splice( index , 1 );
        total += element.Stock;
      } 
    });
    this.updateCart( info.totalCart - total );
  }

  totalPrice(){
    this.total = 0;
    if(  localStorage.getItem('info') != null ) this.products.forEach( element => this.total += ( element.Price * element.Stock ));
  }

  add( pid ){
    var total = 0;
    this.products.forEach(  element  => {
      var pro =  this.productById( element.Id );
      element.Stock += ( element.Id === pid  && element.Stock >= 1 ) ? 1 : 0;
      total += element.Stock;
      element.Quantity =  pro.Stock - element.Stock;
      element.RealQty = pro.Stock;
    });
    this.updateCart(total);
  }

  del( pid ){
    var total = 0;
    this.products.forEach(  element  => {
      var pro =  this.productById( element.Id );
      element.Stock -= ( element.Id === pid  && element.Stock >= 1 ) ? 1 : 0;
      total += element.Stock;
      element.Quantity =  pro.Stock - element.Stock;
      element.RealQty = pro.Stock;
    });
   this.updateCart(total);
  }

  updateCart ( total ) {
    this.totalPrice();
    this.data.changeMessage( total );
    //--- Set info on cookie to update Cart
    localStorage.setItem('info', JSON.stringify({ totalCart : total, items : this.products  }));
  }

  productById( id ){
    var product: IProducts;
    this.allProducts.forEach( element => { if ( element.Id == id ) product = element; } );
    return product;
  }

  isLoged(){
    this.http.get<boolean>( this.serverUrl + '/Customers/isLoged/').subscribe(  data => {
      this.loged = data;
    });
  }

  getAllProducts() {
    this.http.get<IProducts[]>( this.serverUrl + '/Products/all').subscribe(  data => {
      this.allProducts = <IProducts[]>data;
    });
  }

  limitDel( num: number ){
    return (  num == 1 ) ? false : true;
  }

  limitAdd( qty:number , num: number){
    return ( qty > num ) ? true : false;
  }

  messageActive(){
    var flag = false;
    this.products.forEach( element => {
      if( element.Quantity < 0) flag = true;
    });
    return flag;
  }

}
