export interface IProducts {
  Id: string;
  ImageUrl: string;
  Name: string;
  Price: number;
  Details: string;
  Stock: number;
  Quantity: number;
  RealQty: number;
  Category: string;
}
