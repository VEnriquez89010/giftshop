import { CookieManager } from './../cookiemanager';
import { IProducts } from './../products';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { DataService } from '../data.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { ServerUrl } from '../enviroment-variables';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainlist',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainlistComponent implements OnInit {
  totalCart = 0;
  cartProducts: IProducts[] = [];
  selectedProduct: Subject<any> = new Subject;
  allProducts: IProducts[] = [];
  realProducts: IProducts[] = [];
  serverUrl = ServerUrl;
  imagePath;
  itemsPerPage: number = 5;

  constructor( public data: DataService , private cookieService: CookieService , private http: HttpClient , private router: Router , private cookieManager: CookieManager  ) {
    this.getAllProducts();
    this.getAllRealProducts();
   }

   ngOnInit() {
    if( localStorage.getItem('info') != null){
        const info = JSON.parse(localStorage.getItem('info'));
        this.totalCart = info.totalCart;
    }
    this.getAllProducts();
    this.getAllRealProducts();
    this.itemsPerPage =   parseInt(this.cookieService.get("itemsPerPage")) ;
    console.log(this.itemsPerPage);
  }

   newMessage( event ) {
    this.add( event.target.value );
  }

  getpopup( det ) {
    this.selectedProduct.next( det );
  }

  getDetails( id: number ) {
      this.router.navigate(['/info', { id : id }]);
  }

  add( pid ) {
    let flag = false;

    // ---Validate if Json on cookie is empty, in that case work with this.cartProduct list
    this.cartProducts = ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
    this.allProducts.forEach( element => {
       if ( element.Id == pid ) {
        // ---If cartProducts is empty then put element inside list on else
        if ( this.cartProducts.length != 0 ) {
          for ( let cartProd of this.cartProducts ) {
            // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
            if ( cartProd.Id == element.Id ) {
              var product =  this.productById( element.Id );
              cartProd.Stock += 1;
              cartProd.Quantity =  product.Stock - cartProd.Stock;
              cartProd.RealQty = product.Stock;
              flag = false;
              break;
            } else {
              flag = true;
            }
          }
          if (flag) {
            this.addCart( element );
          }
        } else {
          this.addCart( element );
        }
       }
    });
    this.totalCart +=  1;
    //---Send msg to change status Cart
    this.data.changeMessage( this.totalCart.toString() );
    // --- Set info on cookie to update Cart
    localStorage.setItem('info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
  }

  addCart( product ){
    var prod =  this.productById( product.Id );
    product.Quantity =  prod.Stock - product.Stock;
    product.RealQty = prod.Stock;
    this.cartProducts.push( product ) ;
  }

  getAllProducts() {
    this.http.get<IProducts[]>( this.serverUrl + '/Products/productImages').subscribe(  data => {
      this.allProducts = <IProducts[]>data;
      this.allProducts.forEach( element => element.Stock = ( element.Stock > 0 ) ? 1 : 0 );
    });
  }

  getAllRealProducts() {
    this.http.get<IProducts[]>( this.serverUrl + '/Products/all').subscribe(  data => {
      this.realProducts = <IProducts[]>data;
    });
  }

  stock( id ) {
    var cartProduct: IProducts;
   
    this.cartProducts = ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
    this.allProducts.forEach( element => {
       if ( element.Id == id ) {
        // ---If cartProducts is empty then put element inside list on else
        if ( this.cartProducts.length != 0 ) {
          for ( let cartProd of this.cartProducts ) {
            // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
            if ( cartProd.Id == element.Id ) {
              var product =  this.productById( element.Id );
              if(product != undefined){
              //cartProd.Stock += 1;
              cartProd.Quantity =  product.Stock - cartProd.Stock;
              cartProd.RealQty = product.Stock;
              cartProduct = cartProd;
              break;
              }
            } 
          }
        }
       }
    });
    var qty;
    if(cartProduct != undefined ){
      qty = cartProduct.Quantity;
    }else{
      var aux = this.cartProductById( id )
      if( aux != undefined) qty = aux.Stock;
    }
    return ( qty > 0 ) ? true : false ;
  }

  productById( id ){
    var product: IProducts;
    this.realProducts.forEach( element => { if ( element.Id == id ) product = element; } );
    return product;
  }

  cartProductById( id ){
    var product: IProducts;
    this.allProducts.forEach( element => { if ( element.Id == id ) product = element; } );
    return product;
  }



}
