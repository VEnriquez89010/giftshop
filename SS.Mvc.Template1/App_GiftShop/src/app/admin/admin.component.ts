import { Component, OnInit, Input, Output  } from '@angular/core';
import { HttpClient, HttpEventType, HttpResponse, HttpEvent, HttpRequest } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { IProducts } from '../products';
import { ServerUrl } from '../enviroment-variables';
import {FormControl, FormGroup, Validators } from "@angular/forms";
import { EventEmitter } from 'events';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit{
  allProducts: IProducts[] = [];
  selectedEditProduct : Subject<any> = new Subject;
  selectedRemoveProduct : Subject<any> = new Subject;
  addModal : Subject<any> = new Subject;
  serverUrl = ServerUrl;
  urlDocument: any;
  formGroup: FormGroup;
  selectedFiles: FileList;
  currentFileUpload: File;
  @Input() nameFile = 'Chose image';
  progress: { percentage: number} = {percentage: 0};
  urlDocumentController = '/product'
  admin: boolean = false;
  

  constructor( private http: HttpClient ) {   }

  ngOnInit(): void {
    this.getAllProducts( );
    this.validator(); 
    this.isAdmin();
  }

  getpopupEdit( det ) {
    this.selectedEditProduct.next( det );
  }

  getpopupRemove( det ) {
    this.selectedRemoveProduct.next( det );
  }

  getpopupAdd( det ) {
    this.addModal.next( det );
  }

  getAllProducts(  ){
    this.http.get<IProducts[]>( this.serverUrl + '/Products/all').subscribe(  data => this.allProducts = data );
  }

  selectedFile(event){
    const file = event.target.files.item(0);
    if( file.type.match("image.*")){
      this.selectedFiles = event.target.files;
      this.nameFile = event.target.files[0].name;
    }else{
      alert('invalid format!');
    }
  }

  upload(){
    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
   
    this.pushFile( this.serverUrl + '/Products/uploadFile' , this.currentFileUpload ).subscribe( data => {
      if( data.type  ===  HttpEventType.UploadProgress){
        this.progress.percentage = Math.round( 100 * data.loaded / data.total );
      } else if ( data instanceof HttpResponse) {
         console.log('succesful');
         this.urlDocument = data.body;
      }
    });
    this.selectedFiles = undefined;
  }

  pushFile( url: string , file: File ): Observable<HttpEvent<{}>>{
    const formdata: FormData = new FormData();
    formdata.append( file.name , file );
    const req = new HttpRequest( 'POST' , url , formdata , {
      reportProgress: true
    });
    return this.http.request(req);
  }

  validator(){
    this.formGroup = new FormGroup({
      Name: new FormControl('', [
        Validators.required,
        //Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Price: new FormControl('', [
        Validators.required,
        Validators.pattern('^(0|[1-9][0-9]*)$')
        // Validators.minLength(8),
        // Validators.maxLength(20)
      ]),
      Details: new FormControl('', [
        Validators.required,
         Validators.minLength(8),
        // Validators.maxLength(20)
      ]),
      ImageUrl: new FormControl('', [
        // Validators.minLength(8),
        // Validators.maxLength(20)
      ]),
      Stock: new FormControl('', [
        Validators.required,
        Validators.pattern('^(0|[1-9][0-9]*)$')
      ]),
      Category: new FormControl('electronics', [])
    });
  }

  onSubmit() {
    var json = this.formGroup.value;
    json.ImageUrl =  this.urlDocument;
     this.http.post( ServerUrl + '/Products/add' , json ).subscribe( );
    // console.log('Ok');
     location.reload();
  }
 
  onReset() {
    this.formGroup.reset();
  }

  isAdmin(){
    this.http.get( ServerUrl + '/Customers/isAdmin/').subscribe(  data => {
      if (  data == 1 )  this.admin = true;
    });
  }


}
