import { Component, OnInit, Output, EventEmitter, ElementRef } from '@angular/core';
import { IProducts } from '../products';
import { Subject } from 'rxjs';
import { DataService } from '../data.service';
import { CookieService } from 'ngx-cookie-service';
import { ServerUrl } from '../enviroment-variables';
import { HttpClient } from '@angular/common/http';
import { CookieManager } from '../cookiemanager';
import { ActivatedRoute } from '@angular/router';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  totalCart: number = 0;
  cartProducts: IProducts[] = [];
  selectedProduct : Subject<any> = new Subject;
  allProducts:  IProducts[] = [];
  realProducts: IProducts[] = [];
  serverUrl = ServerUrl;
  keyword: string;
  categoy: string;
  imagePath: string;
  itemsPerPage: number;

  constructor(private el: ElementRef , public data: DataService , private cookieService: CookieService , private http: HttpClient , private cookieManager: CookieManager , private route: ActivatedRoute ) { 
    this.getAllRealProducts();
    this.getSearch();
   }

   ngOnInit() {
    if( localStorage.getItem('info') != null){
        var info = JSON.parse(localStorage.getItem('info')); // Json cookie
        this.totalCart = info.totalCart;
    }
    this.getAllRealProducts();
    this.itemsPerPage =   parseInt(this.cookieService.get("itemsPerPage")) ;
  }
  

  getSearch(){
    this.route.params.subscribe( params => {
      this.keyword = params['keyword'] !== undefined ? params['keyword'] : 'none';
      this.categoy = params['categoy'] !== undefined ? params['categoy'] : '';
        this.http.get<IProducts[]>( ServerUrl + '/Products/search/' + this.categoy + '/' + this.keyword ).subscribe(  data => {
          data.forEach( element => element.Stock = ( element.Stock > 0 ) ? 1 : 0 );
          this.allProducts = ( data.length != 0 ) ? data : [];
        });
    });


   
  }

   newMessage( event ) {
    this.add( event.target.value );
    // if( localStorage.getItem('info') != null){
    //   var info =  JSON.parse(localStorage.getItem('info')); // Json cookie
    //   this.totalCart = info.totalCart + 1;
    //   this.data.changeMessage( this.totalCart.toString() )
    //   localStorage.setItem('info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
    // }
  }

  getpopup( det ) {
    this.selectedProduct.next( det );
  }

  add( pid ) {
    let flag = false;

    // ---Validate if Json on cookie is empty, in that case work with this.cartProduct list
    this.cartProducts = ( localStorage.getItem('info') != null ) ? JSON.parse( localStorage.getItem('info') ).items :[];
    this.allProducts.forEach( element => {
       if ( element.Id == pid ) {
        // ---If cartProducts is empty then put element inside list on else
        if ( this.cartProducts.length != 0 ) {
          for ( let cartProd of this.cartProducts ) {
            // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
            if ( cartProd.Id == element.Id ) {
              var product =  this.productById( element.Id );
              cartProd.Stock += 1;
              cartProd.Quantity =  product.Stock - cartProd.Stock;
              cartProd.RealQty = product.Stock;
              flag = false;
              break;
            } else {
              flag = true;
            }
          }
          if (flag)  this.addCart( element );
        } else {
          this.addCart( element );
        }
       }
    });
    this.totalCart +=  1;
    //---Send msg to change status Cart
    this.data.changeMessage( this.totalCart.toString() );
    // --- Set info on cookie to update Cart
    localStorage.setItem('info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
   // this.cookieService.set( 'info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
   // console.log(  localStorage.getItem('info') );
  }

  addCart( product ){
    var prod =  this.productById( product.Id );
    product.Quantity =  prod.Stock - product.Stock;
    product.RealQty = prod.Stock;
    this.cartProducts.push( product ) ;
  }


  getAllRealProducts() {
    this.http.get<IProducts[]>( this.serverUrl + '/Products/all').subscribe(  data => {
      this.realProducts = <IProducts[]>data;
    });
  }

  stock( id ) {
    var cartProduct: IProducts;
   
    this.cartProducts = ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
    this.allProducts.forEach( element => {
       if ( element.Id == id ) {
        // ---If cartProducts is empty then put element inside list on else
        if ( this.cartProducts.length != 0 ) {
          for ( let cartProd of this.cartProducts ) {
            // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
            if ( cartProd.Id == element.Id ) {
              var product =  this.productById( element.Id );
              if(product != undefined){
              //cartProd.Stock += 1;
              cartProd.Quantity =  product.Stock - cartProd.Stock;
              cartProd.RealQty = product.Stock;
              cartProduct = cartProd;
              break;
              }
            } 
          }
        }
       }
    });
    var qty;
    if(cartProduct != undefined ){
      qty = cartProduct.Quantity;
    }else{
      var aux = this.cartProductById( id )
      if( aux != undefined) qty = aux.Stock;
    }
    return ( qty > 0 ) ? true : false ;
  }

   productById( id ){
    var product: IProducts;
    this.realProducts.forEach( element => { if ( element.Id == id ) product = element; } );
    return product;
  }

  cartProductById( id ){
    var product: IProducts;
    this.realProducts.forEach( element => { if ( element.Id == id ) product = element; } );
    return product;
  }


}
