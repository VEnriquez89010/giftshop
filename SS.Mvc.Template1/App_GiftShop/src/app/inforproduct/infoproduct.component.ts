import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProducts } from '../products';
import { ServerUrl } from '../enviroment-variables';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../data.service';
import { CookieManager } from '../cookiemanager';

@Component({
  selector: 'app-infoproduct',
  templateUrl: './infoproduct.component.html',
  styleUrls: ['./infoproduct.component.css']
})

export class InfoProductComponent  {
  serverUrl = ServerUrl;
  product: IProducts;
  totalCart: number = 0;
  cartProducts: IProducts[] = [];
  allProducts: IProducts[] = [];
  realProducts: IProducts[] = [];
  totalCanAdd: number = 0;
  productId: number = 0;

  constructor( public data: DataService , private http: HttpClient , private route: ActivatedRoute , private router: Router , private cookieService: CookieService , private cookieManager: CookieManager ) { 
    this.getAllProducts(); 
    this.getAllRealProducts();
  }
 
   ngOnInit() {
    if( localStorage.getItem('info') != null){
      var info =  JSON.parse(localStorage.getItem('info'));
      this.totalCart = info.totalCart;
    }
    
    this.getAllProducts();
    this.getAllRealProducts();
    this.getDetails( );
  }

  getDetails( ){
    this.route.params.subscribe( params => {
      this.productId = params['id'] !== undefined ? +params['id'] : 0;
      if ( this.productId !== 0 ) {
        this.http.get<IProducts>( this.serverUrl + '/Products/findById/' + this.productId ).subscribe(  data => {
          this.product = data;
          this.totalCanAdd = this.product.Stock;
          var cart = ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
          console.log(cart);
          cart.forEach( element => { if( element.Id == this.product.Id ) this.totalCanAdd = this.product.Stock - element.Stock; });
        });
      }
    });
  }

  // onSearch( categoy: any ) {
  //     this.http.get<IProducts[]>( ServerUrl + '/Products/search/' + categoy + '/none').subscribe(  data => {
  //       data.forEach( element => element.Stock = ( element.Stock > 0 ) ? 1 : 0 );
  //       ( data.length == 0 ) ? this.cookieService.delete( 'search' ) :  this.cookieService.set( 'search', JSON.stringify( { items : data } ));
  //       this.router.navigate(['/search']);
  //       location.reload();
  //     });
  //   }

    newMessage( event , qty ) {
      this.add( event.target.value , qty.value  );
      location.reload();
    }

    add( pid  , qty: number ){
      var flag = false;
      //---Validate if Json on cookie is empty, in that case work with this.cartProduct list
      this.cartProducts = ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
      this.allProducts.forEach( element => {
        element.Stock = +qty;
         if( element.Id == pid ){
          //---If cartProducts is empty then put element inside list on else
          if( this.cartProducts.length != 0 ){
            for ( let cartProd of this.cartProducts ) {
              // --- If cartElement already exist on list, then add another quantity, if not add inside list on else
              if ( cartProd.Id == element.Id ) {
                var product =  this.productById( element.Id );
                cartProd.Stock +=  +qty;
                cartProd.Quantity =  product.Stock - cartProd.Stock;
                cartProd.RealQty = product.Stock;
                flag = false;
                break;
              } else {
                flag = true;
              }
            }
            if ( flag )  this.addCart( element );
          } else {
            this.addCart( element );
          }
         } 
      });
      this.totalCart +=  +qty;
      //---Send msg to change status Cart
      this.data.changeMessage( this.totalCart.toString() );
      // --- Set info on cookie to update Cart
      localStorage.setItem('info', JSON.stringify({ totalCart : this.totalCart , items : this.cartProducts  }));
    }

    addCart( product ){
      var prod =  this.productById( product.Id );
      product.Quantity =  prod.Stock - product.Stock;
      product.RealQty = prod.Stock;
      this.cartProducts.push( product ) ;
    }

    productById( id ){
      var product: IProducts;
      this.realProducts.forEach( element => { if ( element.Id == id ) product = element; } );
      return product;
    }

    cartProductById( id ){
      var product: IProducts;
      this.cartProducts = ( localStorage.getItem('info') != null ) ? JSON.parse(localStorage.getItem('info')).items :[];
      this.cartProducts.forEach( element => { if ( element.Id == id ) product = element; } );
      return product;
    }

    getAllProducts(){
      this.http.get<IProducts[]>( this.serverUrl + '/Products/productImages').subscribe(  data => {
        this.allProducts = <IProducts[]>data;
        this.allProducts.forEach( element => element.Stock = ( element.Stock > 0 ) ? 1 : 0 );
      });
    }

    getAllRealProducts() {
      this.http.get<IProducts[]>( this.serverUrl + '/Products/all').subscribe(  data => {
        this.realProducts = <IProducts[]>data;
      });
    }

    stock( number ){
      return  ( number > 0 ) ? true : false ;
     }

}
