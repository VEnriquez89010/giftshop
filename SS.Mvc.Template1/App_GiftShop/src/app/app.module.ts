import { Login } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { ProductdetailsComponent } from './modals/productdetails/productdetails.component';
import { EditComponent } from './modals/edit/edit.component';
import { RemoveComponent } from './modals/remove/remove.component';
import { AddComponent } from './modals/add/add.component';
import { MainlistComponent } from './main/main.component';
import { RouterModule, Routes } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { DataService } from './data.service';
import { CookieService } from 'ngx-cookie-service';
import { PaymentComponent } from './modals/paymodal/payment.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { SearchComponent } from './search/search.component';
import { MsgComponent } from './msg/msg.component';
import { AdminComponent } from './admin/admin.component';
import { InfoProductComponent } from './inforproduct/infoproduct.component';
import { CookieManager} from './cookiemanager';
import { NgBootstrapFormValidationModule ,  CUSTOM_ERROR_MESSAGES } from 'ng-bootstrap-form-validation';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ERRORS} from "./custom-errors";
import { NgxPaginationModule } from 'ngx-pagination';
import { IOservices } from './ioservices';
import { routing } from './app.routing';
import { TokenInterceptor } from './TokenInterceptor .service';



@NgModule({
  declarations: [
    AppComponent,
    ProductlistComponent,
    ProductdetailsComponent,
    MainlistComponent,
    NavComponent,
    PaymentComponent,
    EditComponent,
    AddComponent,
    RemoveComponent,
    ThankyouComponent,
    SearchComponent,
    AdminComponent,
    MsgComponent,
    InfoProductComponent,
    Login
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    routing,
    NgBootstrapFormValidationModule.forRoot()
  ],
  providers: [ DataService , CookieService, IOservices , CookieManager, {
    provide:   HTTP_INTERCEPTORS  ,
    useClass:  TokenInterceptor ,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
