import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DataService } from "../data.service";
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router"
import { IProducts } from '../products';
import { ServerUrl } from '../enviroment-variables';
import { IOservices } from '../ioservices';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit{
  title = 'Gift Shop';
  totalCart:number;
  allProducts:  IProducts[] = [];
  loged: any;
  admin: boolean = false;
  serverUrl = ServerUrl ;
  itemsPerPage: number = 5;
  isCart: boolean = false;
  @ViewChild('itemsPerPageElement') elementItemsPerPage: ElementRef;

  constructor( public data: DataService  , private http: HttpClient , private ioservices:  IOservices, private router: Router , private cookieService: CookieService) {  this.isLoged(); }

  ngOnInit(): void {
    this.data.currentMessage.subscribe (message => this.totalCart = +message );
    if( localStorage.getItem('info') != null){
      var info =  JSON.parse(localStorage.getItem('info'));
        this.totalCart = +info.totalCart;
        localStorage.setItem('info', JSON.stringify({ totalCart : this.totalCart , items : info.items  }));
    } 
    this.ioservices.inCart.subscribe( event => {
      this.isCart = ( event === 'inCart' ) ? true : false;
    });
    this.itemsPerPage =   parseInt(this.cookieService.get("itemsPerPage")) ;
    this.isLoged();
    this.isAdmin();
    
  }

  changeNumber(){
    this.itemsPerPage = this.elementItemsPerPage.nativeElement.value;
    this.cookieService.set("itemsPerPage" , this.itemsPerPage.toString() );
    location.reload();
  }

  getDetails( searchKeyword: any , categoySelect: any ) {
    var search = (searchKeyword.value == '') ?  'none' : searchKeyword.value.toLowerCase( ) ;
    this.router.navigate(['/search', { keyword : search , categoy: categoySelect.value }]);
    
  }

  getDetailsNav( searchKeyword: any , categoySelect: any ) {
    this.router.navigate(['/search', { keyword : searchKeyword , categoy: categoySelect }]);
  }

  isLoged(){
    this.http.get( ServerUrl + '/Customers/isLoged/').subscribe(  data => {
      this.loged = <any> data;
    });
  }

  isAdmin(){
    this.http.get( ServerUrl + '/Customers/isAdmin/').subscribe(  data => {
      if ( this.loged != '' && data == 1 )  this.admin = true;
    });
  }

  logOff(){
    this.http.get( ServerUrl + '/Account/LogOff').subscribe( );
    localStorage.clear();
    location.reload();
    this.router.navigate(['/']);
  }

}
