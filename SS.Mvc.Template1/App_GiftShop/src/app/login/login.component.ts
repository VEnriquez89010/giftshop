import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DataService } from "../data.service";
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpRequest, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import {Router} from "@angular/router"
import { IProducts } from '../products';
import { ServerUrl } from '../enviroment-variables';
import { IOservices } from '../ioservices';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class Login implements OnInit{
  formGroup: FormGroup;
  headers: HttpHeaders = new HttpHeaders();

  constructor(
    private http: HttpClient ) {  }

  ngOnInit(): void {
    this.initialize ();
  }

  initialize () {
    this.formGroup = new FormGroup({
      Username: new FormControl('', [ ]),
      Password: new FormControl('', [ ])
    });
  }

  onSubmit(){
    var json = this.formGroup.value;
    this.http.post( ServerUrl + '/api/login/authenticate' , json ).subscribe( data => {
      let authToken =  data;  // localStorage.getItem("dealertoken");
      localStorage.setItem( 'token' , authToken.toString());
      // this.request = this.request.clone({
      //     headers: this.request.headers.set('Authorization', 'Bearer '+ authToken)
      //     /* setHeaders: {
      //         Authorization: 'Bearer '+ authToken
      //     } */
      // });
      console.log(data);
    });
  }
}
