import { Injectable } from "@angular/core";
import { ProductlistComponent } from "./productlist/productlist.component";
import { ThankyouComponent } from "./thankyou/thankyou.component";
import { SearchComponent } from "./search/search.component";
import { MsgComponent } from "./msg/msg.component";
import { AdminComponent } from "./admin/admin.component";
import { InfoProductComponent } from "./inforproduct/infoproduct.component";
import { MainlistComponent } from "./main/main.component";
import { Routes, RouterModule } from "@angular/router";
import { Login } from "./login/login.component";



 const appRoutes: Routes = [
      {
        path: 'cart', component: ProductlistComponent
      },
      {
        path: 'thankyou', component: ThankyouComponent
      },
      {
        path: 'search', component: SearchComponent
      },
      {
        path: 'msg', component: MsgComponent
      },
      {
        path: 'login', component: Login
      },
      {
        path: 'DZf1rQHSBkBq4J0qDd54ng', component: AdminComponent
      },
      {
        path: ':id', component: InfoProductComponent
      },
      {
        path: '', component: MainlistComponent
      }
    ];

    export const routing = RouterModule.forRoot(appRoutes );
  
  