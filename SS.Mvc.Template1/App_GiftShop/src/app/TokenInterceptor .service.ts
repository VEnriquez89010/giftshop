import { Injectable } from '@angular/core';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
declare var localStorage : any;

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let allowedUrls =( request.url.indexOf("/api/login/authenticate") < 0 );   

    if(allowedUrls){

        let authToken = localStorage.getItem("token");
        request = request.clone({
            headers: request.headers.set('Authorization', 'Bearer '+ authToken)
        });
    }else {
        console.log('return to login' + request);
    }

     return next.handle(request);
  }
}