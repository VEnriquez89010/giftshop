import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService implements OnInit{
  
  message : string = '0';

  public messageSource = new BehaviorSubject(this.message);
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  ngOnInit(): void {
    this.currentMessage.subscribe(message => this.message = message);
  }

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

}