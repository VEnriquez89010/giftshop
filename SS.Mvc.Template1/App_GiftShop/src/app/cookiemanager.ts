import { DataService } from "./data.service";
import { OnInit, Component, Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { IProducts } from "./products";

@Injectable()
export class CookieManager implements OnInit  {
    
constructor(public data: DataService, private cookieService: CookieService  ) { }
cartProducts: IProducts[] = [];
totalC: number = 0;

    ngOnInit(): void {
        }



    setCookie( totalCart , cartProducts ){
        totalCart +=  1;
        //---Send msg to change status Cart
        this.data.changeMessage( totalCart.toString() );
        // --- Set info on cookie to update Cart
        this.cookieService.set( 'info', JSON.stringify({ totalCart : totalCart , items : cartProducts  }));
        //console.log(totalCart);
        return totalCart;
      }
     
}